<<<<<<< HEAD
'use strict'
const Socket = require('./js/socket.js');
const Theater = require('./js/theater.js');

const Plane = require('./js/plane.js');
const Center = require('./js/center.js');
const Radar = require('./js/radar.js');
const Missile = require('./js/missile.js');
const Vehicle = require('./js/vehicle.js');

const fs = require('fs');
const Canvas = require('canvas');
const CONFIG = require("./js/config.js");

let planeImage;
let radarImage;
let centerImage;
let samImage;
let missileImage;

loadImages();
// Frame initializer running 60hz 500 frames.
var frames = { 
	speed: (1000/144),
	count: 0,
	max: -1,
	timer:'',
	run:function(func){
		this.timer = setInterval(func, this.speed);
	},
	start:function(func, speed = 100){
		this.speed = speed;
		this.run(func);
	}
}


var socket = new Socket()
var send = socket.server.send;
socket.startServer().then(() => {
});

var theater = new Theater(0, "west", 1, "east"); //# of Blu, Blu Spawn Point, # of Red, Red Spawn Point

theater.init().then(() => {
	var blufor = theater.blufor;
	var redfor = theater.redfor;

	blufor.spawnCenter(1);
	blufor.spawnRadar(1);
	blufor.spawnVehicle(1);

	for (var x = 0; x < blufor.vehicles.length; x++) { // Vehicle's fire event telling the missile to fire and pushing the missile to the theater for render.
		blufor.vehicles[x].events.on("fire", (obj) => {
			theater.unitCategory.weapon.push(obj);
			theater.units.push(obj);
			obj.events.removeAllListeners();
			obj.events.on('explode', (ref) => {
				var location = { posVector: ref.posVector };
				theater.kill(ref);
				theater.checkKill(location);
			})
		});
	}

    for (let j = 0; j < blufor.vehicles.length; j++) { // Link-16 simple version to distribute information of locked targets on a frequency
        var unit = blufor.vehicles[j];
        unit.run(); //preload vehicle with weapons
        unit.link.on(CONFIG.RADIO_FREQ.freq + CONFIG.RADIO_FREQ.band, (src) => {
            var obj = blufor.vehicles[j];
            var target = src.target;
            var posVector = target.posVector;
            var unitHeading = obj.calculateHeading(target);
            var unitDistance = obj.calculateDistance(target);
				var sourceDistance = obj.calculateDistance(src);
            if ((!obj.target || obj.lockAttempt > CONFIG.LOCK_LATENCY) && unitDistance < obj.range && sourceDistance <= CONFIG.MAX_CONNECTION_RANGE) {
				obj.lock(target.id, posVector, unitHeading, unitDistance); // Assigns vehicles targets to lock
			}
		})
	}

	for (var i = 0; i < redfor.planes.length; i++) { //Sets redfor to lock random blufor units.
		var obj = redfor.planes[i];
		theater.randomLock(obj);
	}

	console.log(blufor.planes.length + " Blufor Planes");
	console.log(blufor.vehicles.length + " Blufor Sam Sites");
	console.log(blufor.radars.length + " Blufor Radars");
	console.log(blufor.centers.length + " BluFor Centers");
	console.log("----------");
	console.log(redfor.planes.length + " Redfor Planes\n");

	//render code in here
	frames.start(() => {

		for (var x = 0; x < blufor.vehicles.length; x++) { //Execute SAM sites functions such as firing missiles/bullets and reloading when fed data from radars.
			var vehicle = blufor.vehicles[x];
			if (vehicle.power === true) {
				vehicle.run();
			}
		}

		for (var x = 0; x < blufor.radars.length; x++) { //Execute Radar functions, Theater checks if the radar is pointed at a target eg. Radar -> Plane
			var radar = blufor.radars[x];
			if (radar.power === true) {
				radar.run();
				theater.checkScan(radar);
				//console.log(radar.name+" | Heading: "+radar.heading+"°");
			}
		}

		for (var x = 0; x < theater.unitCategory.weapon.length; x++) { //Execute active weapons functions eg. missiles, bullets, etc.
			var obj = theater.unitCategory.weapon[x];
			obj.run();
			if (!theater.checkScan(obj)) {
				theater.kill(obj);
			}
		}

		for (let p = 0; p < redfor.planes.length; p++) { //Execute all plane functions
			redfor.planes[p].run();
		}

		//send all clients new object locations
		pushSocket();

		//code for canvas here
		//drawImages();

		//term program after specific amount of frames
		frames.count++;
		if (frames.count == frames.max) {
			process.exit();
		}

	}, this.speed);
});



async function pushSocket() {
	/*
    for (let i = 0; i < theater.units.length; i++) {
        var unit = theater.units[i];
    }
	*/
    send(theater.units);
}

async function loadImages(){
	planeImage = await Canvas.loadImage(`./assets/plane.png`);
	radarImage = await Canvas.loadImage(`./assets/radar.png`);
	centerImage = await Canvas.loadImage(`./assets/center.png`);
	samImage = await Canvas.loadImage(`./assets/sam.png`);
	missileImage = await Canvas.loadImage(`./assets/missile.png`);
}

async function drawImages(){
	const canvas = Canvas.createCanvas(CONFIG.FIELD_SIZE, CONFIG.FIELD_SIZE)
	const context = canvas.getContext('2d');
	//start of code that draws the canvas and outputs to file
	
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,CONFIG.FIELD_SIZE, CONFIG.FIELD_SIZE);
	for(let out=0;out<theater.units.length;out++){
		//draw background rectangle
		context.fillStyle = `rgb(${theater.units[out].rbgVector.r},${theater.units[out].rbgVector.g},${theater.units[out].rbgVector.b})`;
		context.fillRect(theater.units[out].posVector.x,theater.units[out].posVector.y,10,10);
		//draw object based on what it is shape
		if(theater.units[out] instanceof Plane){
			context.drawImage(planeImage,theater.units[out].posVector.x,theater.units[out].posVector.y);
		}
		else if(theater.units[out] instanceof Radar){
			context.drawImage(radarImage,theater.units[out].posVector.x,theater.units[out].posVector.y);
		}
		else if(theater.units[out] instanceof Center){
			context.drawImage(centerImage,theater.units[out].posVector.x,theater.units[out].posVector.y);
		}
		else if(theater.units[out] instanceof Vehicle){
			context.drawImage(samImage,theater.units[out].posVector.x,theater.units[out].posVector.y);
		}
		else if(theater.units[out] instanceof Missile){
			context.drawImage(missileImage,theater.units[out].posVector.x,theater.units[out].posVector.y);
		}
		//draw movement line
		context.beginPath();
		context.setLineDash([]);
		context.moveTo(theater.units[out].posVector.x + 5,theater.units[out].posVector.y + 5);
		context.lineTo(theater.units[out].posVector.x + theater.units[out].velVector.x,theater.units[out].posVector.y + theater.units[out].velVector.y);
		context.strokeStyle = 'black';
		context.stroke();
		//draw circle around target
		if(theater.units[out].hasOwnProperty('target')){
			if(theater.units[out].target != null){
				//draw movement line
				context.beginPath();
				context.moveTo(theater.units[out].posVector.x + 5,theater.units[out].posVector.y + 5);
				context.lineTo(theater.units[out].target.posVector.x+5,theater.units[out].target.posVector.y + 5);
				context.strokeStyle = 'black';
				context.stroke();
				//draw circle around target
				context.beginPath();
				context.strokeStyle = 'red';
				context.arc(theater.units[out].target.posVector.x + 5,theater.units[out].target.posVector.y + 5,10,0, 2*Math.PI);
				context.stroke();
			}
			else{
				//draw scan cone
				//draw line to cone base
				context.beginPath();
				context.moveTo(theater.units[out].posVector.x + 5,theater.units[out].posVector.y + 5);
				//get base of scrubbed cone like in code
				let mainBase = theater.units[out].getBaseFromHeading(theater.units[out].heading, theater.units[out].range);
				context.lineTo(mainBase.x,mainBase.y);
				context.strokeStyle = 'red';
				context.stroke();
				//draw line to cone base at angle
				context.beginPath();
				context.moveTo(theater.units[out].posVector.x + 5,theater.units[out].posVector.y + 5);
				let minBase = theater.units[out].getBaseFromHeading(theater.units[out].heading - theater.units[out].scanAngle, theater.units[out].range);
				context.lineTo(minBase.x,minBase.y);
				context.strokeStyle = 'green';
				context.stroke();
				//draw at opposite of cone
				context.beginPath();
				context.moveTo(theater.units[out].posVector.x + 5,theater.units[out].posVector.y + 5);
				let maxBase = theater.units[out].getBaseFromHeading(theater.units[out].heading + theater.units[out].scanAngle, theater.units[out].range);
				context.lineTo(maxBase.x,maxBase.y);
				context.strokeStyle = 'green';
				context.stroke();
			}
		}
		//write Z height
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`+${theater.units[out].posVector.z.toFixed(2)}`,theater.units[out].posVector.x + 11,theater.units[out].posVector.y + 11);
	}
	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./radarImages/image${frames.count}.png`,buffer);
	//end of canvas code
}
=======
>>>>>>> development
