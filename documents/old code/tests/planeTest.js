'use strict'
const fs = require('fs');
const { createCanvas } = require('canvas');
const boid = require('../js/boid.js');

const FIELD_SIZE = 500;
const MAX_PLANES = 5;
const MAX_TURNS = 500;
const MAX_CONNECTION_RANGE = 300;
const MAX_SPEED = 20;
const Plane = require('../js/plane.js');

let startTime = Date.now();

console.log('creating planes');
//init of the planes
let planeArray = new Array();
for(let i=0;i<MAX_PLANES;i++){
	let position_x = Math.random() * FIELD_SIZE;
	let position_y = Math.random() * FIELD_SIZE;
	let position_z = Math.random() * FIELD_SIZE;
	planeArray.push(new Plane(`${Math.floor(position_x)}${Math.floor(position_y)}${Math.floor(position_z)}`,position_x, position_y, position_z));
}
//add initial connections
console.log('creating original connections')
for(let i=0;i<planeArray.length;i++){
	for(let j=0;j<planeArray.length;j++){
		if(i == j){
			//do nothing since plane doesnt want to reference itself
		}
		else{
			planeArray[i].addNewConnection(planeArray[j]);
		}
	}
}

const canvas = createCanvas(FIELD_SIZE,FIELD_SIZE)
const context = canvas.getContext('2d');

console.log('starting frames');
let frameReview = `Field Size: ${FIELD_SIZE}, Planes: ${MAX_PLANES}, Turns: ${MAX_TURNS}\n`;
//let frameReview = ``;
//frames to run
for(let i=0;i<MAX_TURNS;i++){
	frameReview += `frame ${i}\n`;

	//updateRandomMovements();
	boid.moveAllBoids(planeArray, FIELD_SIZE, MAX_SPEED);
	//updateVelMovements();
	
	//start of code that draws the canvas and outputs to file
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,FIELD_SIZE,FIELD_SIZE);
	for(let out=0;out<planeArray.length;out++){
		frameReview += planeArray[out].printPrettyPlaneInfo();
		frameReview += '\n';
		for(let lin=0;lin<planeArray[out].link_connections.length;lin++){
			if(planeArray[out].link_connections[lin].distance > MAX_CONNECTION_RANGE){
				context.beginPath();
				context.setLineDash([5, 15]);
				context.moveTo(planeArray[out].posVector.x + 2,planeArray[out].posVector.y + 2);
				context.lineTo(planeArray[out].link_connections[lin].object.posVector.x + 2,planeArray[out].link_connections[lin].object.posVector.y + 2);
				context.strokeStyle = 'red';
				context.stroke();
			}
			else{
				//draw connection lines
				context.beginPath();
				context.setLineDash([]);
				context.moveTo(planeArray[out].posVector.x + 2,planeArray[out].posVector.y + 2);
				context.lineTo(planeArray[out].link_connections[lin].object.posVector.x + 2,planeArray[out].link_connections[lin].object.posVector.y + 2);
				context.strokeStyle = 'green';
				context.stroke();
			}
			let midpointX = (planeArray[out].posVector.x + 2 + planeArray[out].link_connections[lin].object.posVector.x + 2) / 2;
			let midpointY = (planeArray[out].posVector.y + 2 + planeArray[out].link_connections[lin].object.posVector.y + 2) / 2;
			context.font = '12px monospace';
			context.fillStyle = `black`;
			context.fillText(`${planeArray[out].link_connections[lin].distance.toFixed(2)}`,midpointX,midpointY);
		}
		//draw plane shape
		context.fillStyle = `rgb(${planeArray[out].rbgVector.r},${planeArray[out].rbgVector.g},${planeArray[out].rbgVector.b})`;
		context.fillRect(planeArray[out].posVector.x,planeArray[out].posVector.y,5,5);
		//draw movement line
		context.beginPath();
		context.setLineDash([]);
		context.moveTo(planeArray[out].posVector.x + 2,planeArray[out].posVector.y + 2);
		context.lineTo(planeArray[out].posVector.x + planeArray[out].velVector.x,planeArray[out].posVector.y + planeArray[out].velVector.y);
		context.strokeStyle = 'black';
		context.stroke();
		//write Z height
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`+${planeArray[out].posVector.z.toFixed(2)}`,planeArray[out].posVector.x + 6,planeArray[out].posVector.y + 6);
	}
	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./planeImages/image${i}.png`,buffer);
	//end of drawing and outputting code
	process.stdout.write(`loading... ${Math.floor((i/MAX_TURNS)*100)}%\r`);
}
let endTime = Date.now();
process.stdout.write(`loading... Done!\n`);
let timeElapsed = endTime - startTime;
console.log('Time: ' + timeElapsed + 'ms');
fs.writeFileSync('./output.txt',frameReview);

function updateRandomMovements(){
	//update all positions
	for(let p=0;p<planeArray.length;p++){
		planeArray[p].moveRandomPos(FIELD_SIZE);
	}
	//update distances
	for(let d=0;d<planeArray.length;d++){
		planeArray[d].updateDistance();
	}
}
function updateVelMovements(){
	//update all positions
	for(let p=0;p<planeArray.length;p++){
		planeArray[p].moveWithVelocity(FIELD_SIZE);
	}
	//update distances
	for(let d=0;d<planeArray.length;d++){
		planeArray[d].updateDistance();
	}
}
