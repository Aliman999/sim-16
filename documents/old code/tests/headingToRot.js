'use strict'
const Plane = require('../js/plane.js');
const MAX_TURNS = 360;

let testUnit = new Plane("unit",0,0,0);
let heading = 0;

for(let i=0;i<MAX_TURNS;i++){
	console.log(testUnit.calculateRotVector(heading));
	heading += 1;
}