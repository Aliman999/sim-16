'use strict'
const Theater = require('./../js/theater.js');
const Vehicle = require('./../js/vehicle.js');
const Canvas = require('canvas');
const CONFIG = require("./../js/config.js");
const fs = require('fs');


var frames = {
    speed: 30,
    count: 0,
    max: 2,
    timer: '', //Interval init
    run: function (func) {
        this.timer = setInterval(func, this.speed);
    },
    start: function (func, speed = 100) {
        this.speed = speed;
        this.run(func);
    }
}

var theater = new Theater(0, "northwest", 0, "south"); //# of Blu, Blu Spawn Point, # of Red, Red Spawn Point

theater.init().then(() => {
    var blufor = theater.blufor;
    var redfor = theater.redfor;

    blufor.spawnVehicle(100);

    //render code in here
    frames.start(() => {
        //code for canvas here
        drawImages();

        //term program after specific amount of frames
        frames.count++;
        if (frames.count == frames.max) {
            process.exit();
        }

    }, this.speed);
});

async function drawImages() {
    const canvas = Canvas.createCanvas(CONFIG.FIELD_SIZE, CONFIG.FIELD_SIZE)
    const context = canvas.getContext('2d');
    //start of code that draws the canvas and outputs to file
    let samImage = await Canvas.loadImage(`./../assets/sam.png`);
    context.fillStyle = '#FFFFFF';
    context.fillRect(0, 0, CONFIG.FIELD_SIZE, CONFIG.FIELD_SIZE);
    for (let out = 0; out < theater.units.length; out++) {
        for (let lin = 0; lin < theater.units[out].link_connections.length; lin++) {
            if (theater.units[out].link_connections[lin].distance > CONFIG.MAX_CONNECTION_RANGE) {
                context.beginPath();
                context.setLineDash([5, 15]);
                context.moveTo(theater.units[out].posVector.x + 5, theater.units[out].posVector.y + 5);
                context.lineTo(theater.units[out].link_connections[lin].object.posVector.x + 5, theater.units[out].link_connections[lin].object.posVector.y + 5);
                context.strokeStyle = 'red';
                context.stroke();
            }
            else {
                //draw connection lines
                context.beginPath();
                context.setLineDash([]);
                context.moveTo(theater.units[out].posVector.x + 5, theater.units[out].posVector.y + 5);
                context.lineTo(theater.units[out].link_connections[lin].object.posVector.x + 5, theater.units[out].link_connections[lin].object.posVector.y + 5);
                context.strokeStyle = 'green';
                context.stroke();
            }
            let midpointX = (theater.units[out].posVector.x + 5 + theater.units[out].link_connections[lin].object.posVector.x + 5) / 2;
            let midpointY = (theater.units[out].posVector.y + 5 + theater.units[out].link_connections[lin].object.posVector.y + 5) / 2;
            context.font = '12px monospace';
            context.fillStyle = `black`;
            context.fillText(`${theater.units[out].link_connections[lin].distance.toFixed(2)}`, midpointX, midpointY);
        }
        //draw background rectangle
        context.fillStyle = `rgb(${theater.units[out].rbgVector.r},${theater.units[out].rbgVector.g},${theater.units[out].rbgVector.b})`;
        context.fillRect(theater.units[out].posVector.x, theater.units[out].posVector.y, 10, 10);
        //draw object based on what it is shape
        if (theater.units[out] instanceof Vehicle) {
            context.drawImage(samImage, theater.units[out].posVector.x, theater.units[out].posVector.y);
        }
        //draw movement line
        context.beginPath();
        context.setLineDash([]);
        context.moveTo(theater.units[out].posVector.x + 5, theater.units[out].posVector.y + 5);
        context.lineTo(theater.units[out].posVector.x + theater.units[out].velVector.x, theater.units[out].posVector.y + theater.units[out].velVector.y);
        context.strokeStyle = 'black';
        context.stroke();
        //draw circle around target
        if (theater.units[out].hasOwnProperty('target')) {
            if (theater.units[out].target != null) {
                context.beginPath();
                context.strokeStyle = 'red';
                context.arc(theater.units[out].posVector.x + theater.units[out].velVector.x, theater.units[out].posVector.y + theater.units[out].velVector.y, 10, 0, 2 * Math.PI);
                context.stroke();
            }
        }
        //write Z height
        context.font = '12px monospace';
        context.fillStyle = `black`;
        context.fillText(`+${theater.units[out].posVector.z.toFixed(2)}`, theater.units[out].posVector.x + 11, theater.units[out].posVector.y + 11);
    }
    const buffer = canvas.toBuffer('image/png');
    fs.writeFileSync(`./locationImages/image${frames.count}.png`, buffer);
    //end of canvas code
}