'use strict'
const Plane = require('../js/plane.js');
const Center = require('../js/center.js');
const fs = require('fs');
const Canvas = require('canvas');

const FIELD_SIZE = 500;
const MAX_CONNECTION_RANGE = 300;
const MAX_TURNS = 200;

let startTime = Date.now();

console.log('creating planes');
//init of the planes
let unitArray = new Array();
unitArray.push(new Plane("plane",200,100,100));
unitArray.push(new Plane("plane2",300,200,400));

console.log('starting frames');
doFrames();

async function doFrames(){
	//frames to run
	for(let i=0;i<MAX_TURNS;i++){
		await drawImages(i);
		//trigger movements
		unitArray[0].velVector = unitArray[0].goToTarget(unitArray[1].posVector, 25, 30, 100, FIELD_SIZE);
		unitArray[1].velVector = unitArray[1].avoidPosition(unitArray[0].posVector, 25, 30, 100, FIELD_SIZE);
		//inject randomness to movements
		unitArray[0].velVector = unitArray[0].injectRandomMovement(5);
		unitArray[1].velVector = unitArray[1].injectRandomMovement(5);
		//update both positions
		unitArray[1].performMovement();
		unitArray[0].performMovement();
		
		//end of drawing and outputting code
		process.stdout.write(`loading... ${Math.floor((i/MAX_TURNS)*100)}%\r`);
	}
	let endTime = Date.now();
	process.stdout.write(`loading... Done!\n`);
	let timeElapsed = endTime - startTime;
	console.log('Time: ' + timeElapsed + 'ms');
}

async function drawImages(i){
	const canvas = Canvas.createCanvas(FIELD_SIZE,FIELD_SIZE)
	const context = canvas.getContext('2d');
	//start of code that draws the canvas and outputs to file
	let planeImage = await Canvas.loadImage(`../assets/plane.png`);
	let centerImage = await Canvas.loadImage(`../assets/center.png`);
	//start of code that draws the canvas and outputs to file
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,FIELD_SIZE,FIELD_SIZE);
	for(let out=0;out<unitArray.length;out++){
		//draw plane shape
		context.fillStyle = `rgb(${unitArray[out].rbgVector.r},${unitArray[out].rbgVector.g},${unitArray[out].rbgVector.b})`;
		context.fillRect(unitArray[out].posVector.x,unitArray[out].posVector.y,10,10);
		//draw object based on what it is shape
		if(unitArray[out] instanceof Plane){
			context.drawImage(planeImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		else if(unitArray[out] instanceof Center){
			context.drawImage(centerImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		//draw movement line
		context.beginPath();
		context.setLineDash([]);
		context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
		context.lineTo(unitArray[out].posVector.x + unitArray[out].velVector.x,unitArray[out].posVector.y + unitArray[out].velVector.y);
		context.strokeStyle = 'black';
		context.stroke();
		//write Z height
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`+${unitArray[out].posVector.z.toFixed(2)}`,unitArray[out].posVector.x + 11,unitArray[out].posVector.y + 11);
	}
	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./trackTest/image${i}.png`,buffer);
}