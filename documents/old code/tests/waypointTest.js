'use strict'
const Plane = require('../js/plane.js');
const vector = require('../js/vector.js');
const fs = require('fs');
const Canvas = require('canvas');

const FIELD_SIZE = 250;

let startTime = Date.now();

console.log('creating planes');
let plane = new Plane("plane", 10, 10, 100);

plane.addWaypoint(new vector.Vector(240,10,100));
plane.addWaypoint(new vector.Vector(240,240,100));
plane.addWaypoint(new vector.Vector(10,240,100));
plane.addWaypoint(new vector.Vector(10,10,240));
plane.addWaypoint(new vector.Vector(240,240,0));

console.log('starting frames');
doFrames();

async function doFrames(){
	//frames to run
	//get first waypoint
	let i=0;
	plane.currentWaypoint = plane.getNextWaypoint();
	while(plane.waypoints.length != 0 || !vector.vectorEquality(plane.posVector, plane.currentWaypoint)){
		plane.velVector = plane.goToTarget(plane.currentWaypoint,10,5,1,FIELD_SIZE);
		plane.performMovement();
		await drawImages(i);
		
		if(vector.vectorEquality(plane.posVector, plane.currentWaypoint)){
			if(plane.waypoints.length == 0)
				break;
			plane.currentWaypoint = plane.getNextWaypoint();
		}
		//end of drawing and outputting code
		i++;
	}
	let endTime = Date.now();
	let timeElapsed = endTime - startTime;
	console.log('Time: ' + timeElapsed + 'ms');
}

async function drawImages(i){
	const canvas = Canvas.createCanvas(FIELD_SIZE,FIELD_SIZE)
	const context = canvas.getContext('2d');
	//start of code that draws the canvas and outputs to file
	let planeImage = await Canvas.loadImage(`../assets/plane.png`);
	//start of code that draws the canvas and outputs to file
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,FIELD_SIZE,FIELD_SIZE);
	//draw plane shape
	context.fillStyle = `rgb(${plane.rbgVector.r},${plane.rbgVector.g},${plane.rbgVector.b})`;
	context.fillRect(plane.posVector.x,plane.posVector.y,10,10);
	//draw object based on what it is shape
	context.drawImage(planeImage,plane.posVector.x,plane.posVector.y);
	//draw movement line
	context.beginPath();
	context.setLineDash([]);
	context.moveTo(plane.posVector.x + 5,plane.posVector.y + 5);
	context.lineTo(plane.posVector.x + plane.velVector.x,plane.posVector.y + plane.velVector.y);
	context.strokeStyle = 'black';
	context.stroke();
	//write Z height
	context.font = '12px monospace';
	context.fillStyle = `black`;
	context.fillText(`+${plane.posVector.z.toFixed(2)}`,plane.posVector.x + 11,plane.posVector.y + 11);

	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./waypoints/image${i}.png`,buffer);
}