'use strict'
const Plane = require('../js/plane.js');
const fs = require('fs');
const Canvas = require('canvas');
const vector = require('../js/vector.js');

const FIELD_SIZE = 200;
const MAX_CONNECTION_RANGE = 300;
const MAX_TURNS = 360;

let startTime = Date.now();

console.log('creating planes');
//init of the planes

let unitArray = new Array();

unitArray.push(new Plane("plane1",10,10,50));
unitArray.push(new Plane("plane2",90,90,50));

let beamLength = 50;
let maxAngle = 30;

let planeHit = new Array();
planeHit.push(false);
planeHit.push(false);
unitArray[1].scanHeading = 90;
console.log('starting frames');

unitArray

doFrames();

async function doFrames(){
	//frames to run
	for(let i=0;i<MAX_TURNS;i++){
		//move the planes
		unitArray[0].moveWithVelocity(FIELD_SIZE);
		unitArray[1].moveWithVelocity(FIELD_SIZE);
		//check their cones
		planeHit[1] = unitArray[1].targetInCone(unitArray[1].posVector, maxAngle, unitArray[0], unitArray[1].scanHeading, beamLength);
		planeHit[0] = unitArray[0].targetInCone(unitArray[0].posVector, maxAngle, unitArray[1], unitArray[0].scanHeading, beamLength);
		
		
		await drawImages(i);
		
		unitArray[0].scanHeading += 1;
		unitArray[0].scanHeading = unitArray[0].scanHeading % 360;
		unitArray[1].scanHeading += 1;
		unitArray[1].scanHeading = unitArray[1].scanHeading % 360;
		
		
		//end of drawing and outputting code
	}
	let endTime = Date.now();
	let timeElapsed = endTime - startTime;
	console.log('Time: ' + timeElapsed + 'ms');
}

async function drawImages(i){
	const canvas = Canvas.createCanvas(FIELD_SIZE,FIELD_SIZE)
	const context = canvas.getContext('2d');
	//start of code that draws the canvas and outputs to file
	let planeImage = await Canvas.loadImage(`../assets/plane.png`);
	let missileImage = await Canvas.loadImage(`../assets/missile.png`);
	//start of code that draws the canvas and outputs to file
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,FIELD_SIZE,FIELD_SIZE);
	for(let out=0;out<unitArray.length;out++){
		//draw plane shape
		context.fillStyle = `rgb(${unitArray[out].rbgVector.r},${unitArray[out].rbgVector.g},${unitArray[out].rbgVector.b})`;
		context.fillRect(unitArray[out].posVector.x,unitArray[out].posVector.y,10,10);
		//draw object based on what it is shape
		if(unitArray[out] instanceof Plane){
			context.drawImage(planeImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		else if(unitArray[out] instanceof Missile){
			context.drawImage(missileImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		
		//draw scan cone
		//draw line to cone base
		context.beginPath();
		context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
		//get base of scrubbed cone like in code
		let mainBase = unitArray[out].getBaseFromHeading(unitArray[out].scanHeading, beamLength);
		context.lineTo(mainBase.x,mainBase.y);
		context.strokeStyle = 'red';
		context.stroke();
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`${mainBase.x.toFixed(2)}, ${mainBase.y.toFixed(2)}`,mainBase.x,mainBase.y);
		
		//draw line to cone base at angle
		context.beginPath();
		context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
		let minBase = unitArray[out].getBaseFromHeading(unitArray[out].scanHeading - maxAngle, beamLength);
		context.lineTo(minBase.x,minBase.y);
		context.strokeStyle = 'green';
		context.stroke();
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`${minBase.x.toFixed(2)}, ${minBase.y.toFixed(2)}`,minBase.x,minBase.y);
		//draw at opposite of cone
		context.beginPath();
		context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
		let maxBase = unitArray[out].getBaseFromHeading(unitArray[out].scanHeading + maxAngle, beamLength);
		context.lineTo(maxBase.x,maxBase.y);
		context.strokeStyle = 'green';
		context.stroke();
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`${maxBase.x.toFixed(2)}, ${maxBase.y.toFixed(2)}`,maxBase.x,maxBase.y);

		//circle if enemy found
		if(planeHit[out]){
				context.beginPath();
				context.strokeStyle = 'red';
				let currentEnemy = (out + 1)%2;
				context.arc(unitArray[currentEnemy].posVector.x + 5,unitArray[currentEnemy].posVector.y + 5,10,0, 2*Math.PI);
				context.stroke();
		}
		//draw movement line
		context.beginPath();
		context.setLineDash([]);
		context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
		context.lineTo(unitArray[out].posVector.x + unitArray[out].velVector.x,unitArray[out].posVector.y + unitArray[out].velVector.y);
		context.strokeStyle = 'black';
		context.stroke();
		//write Z height
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`+${unitArray[out].posVector.z.toFixed(2)}`,unitArray[out].posVector.x + 11,unitArray[out].posVector.y + 11);
	}
	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./planeRadars/image${i}.png`,buffer);
}