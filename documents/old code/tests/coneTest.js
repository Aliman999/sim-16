'use strict'
const Plane = require('../js/plane.js');
const Radar = require('../js/radar.js');
const Missile = require('../js/missile.js');
const fs = require('fs');
const Canvas = require('canvas');
const vector = require('../js/vector.js');

const Socket = require('../js/socket.js');

const FIELD_SIZE = 500;
const MAX_CONNECTION_RANGE = 300;
const MAX_TURNS = 1;

let startTime = Date.now();


console.log('creating planes');
//init of the planes
let unitArray = new Array();
unitArray.push(new Radar("radar", 0, 250, "radar"));		    //0
unitArray.push(new Plane("plane", 50, 250, 0, "f_16"));		    //1
unitArray.push(new Plane("plane2", 100, 250, 50, "f_16"));		//2
unitArray.push(new Plane("plane3", 150, 250, 100, "f_16"));     //3
unitArray.push(new Plane("plane4", 200, 250, 150, "f_16"));   	//4

let scanHeight = 50;
unitArray[0].range = 200;
unitArray[0].scanAngle = 45;
let found = [false,false,false,false,false];

unitArray[0].heading = 0;

console.log('starting frames');

var socket = new Socket()
var send = socket.server.send;
socket.startServer().then(() => {
	doFrames();
});

var frames = {
	speed: (1000 / 144),
	count: 0,
	max: -1,
	timer: '',
	run: function (func) {
		this.timer = setInterval(func, this.speed);
	},
	start: function (func, speed = 100) {
		this.speed = speed;
		this.run(func);
	}
}

async function doFrames() {
	//frames to run
	frames.start(() => {
		//simply console log if within the range
		for (let t = 1; t < unitArray.length; t++) {
			let result = unitArray[0].targetInCone(unitArray[0].posVector, unitArray[0].scanAngle, unitArray[t], unitArray[0].heading, unitArray[0].range, scanHeight);
			found[t] = result;
			if (found[t]) {
				//console.log(`Found`);
			}
		}
		//await drawImages(i);
		pushSocket();
		unitArray[0].heading += 1;
		unitArray[0].heading = unitArray[0].heading % 360;

		//end of drawing and outputting code
	}, frames.speed);
	//let endTime = Date.now();
	//let timeElapsed = endTime - startTime;
	//console.log('Time: ' + timeElapsed + 'ms');
}
async function pushSocket() {
	send(unitArray);
}
/*
async function drawImages(i){
	const canvas = Canvas.createCanvas(FIELD_SIZE,FIELD_SIZE)
	const context = canvas.getContext('2d');
	//start of code that draws the canvas and outputs to file
	let planeImage = await Canvas.loadImage(`../assets/plane.png`);
	let radarImage = await Canvas.loadImage(`../assets/radar.png`);
	//start of code that draws the canvas and outputs to file
	context.fillStyle = '#FFFFFF';
	context.fillRect(0,0,FIELD_SIZE,FIELD_SIZE);
	for(let out=0;out<unitArray.length;out++){
		//draw plane shape
		context.fillStyle = `rgb(${unitArray[out].rbgVector.r},${unitArray[out].rbgVector.g},${unitArray[out].rbgVector.b})`;
		context.fillRect(unitArray[out].posVector.x,unitArray[out].posVector.y,10,10);
		//draw object based on what it is shape
		if(unitArray[out] instanceof Plane){
			context.drawImage(planeImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		else if(unitArray[out] instanceof Radar){
			context.drawImage(radarImage,unitArray[out].posVector.x,unitArray[out].posVector.y);
		}
		if(out == 0){
			//draw scan cone
			//draw line to cone base
			context.beginPath();
			context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
			
			//get base of scrubbed cone like in code
			let mainBase = unitArray[out].getBaseFromHeading(unitArray[out].heading, unitArray[out].range,scanHeight);
			context.lineTo(mainBase.x,mainBase.y);
			context.strokeStyle = 'red';
			context.stroke();
			context.font = '12px monospace';
			context.fillStyle = `black`;
			context.fillText(`${mainBase.x.toFixed(2)}, ${mainBase.y.toFixed(2)}, ${mainBase.z.toFixed(2)}`,mainBase.x,mainBase.y);
			
			//draw line to cone base at angle
			context.beginPath();
			context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
			let minBase = unitArray[out].getBaseFromHeading(unitArray[out].heading - (unitArray[out].scanAngle / 2), unitArray[out].range,scanHeight);
			context.lineTo(minBase.x,minBase.y);
			context.strokeStyle = 'green';
			context.stroke();
			context.font = '12px monospace';
			context.fillStyle = `black`;
			context.fillText(`${minBase.x.toFixed(2)}, ${minBase.y.toFixed(2)}, ${minBase.z.toFixed(2)}`,minBase.x,minBase.y);
			
			//draw at opposite of cone
			context.beginPath();
			context.moveTo(unitArray[out].posVector.x + 5,unitArray[out].posVector.y + 5);
			let maxBase = unitArray[out].getBaseFromHeading(unitArray[out].heading + (unitArray[out].scanAngle / 2), unitArray[out].range,scanHeight);
			context.lineTo(maxBase.x,maxBase.y);
			context.strokeStyle = 'green';
			context.stroke();
			context.font = '12px monospace';
			context.fillStyle = `black`;
			context.fillText(`${maxBase.x.toFixed(2)}, ${maxBase.y.toFixed(2)}, ${maxBase.z.toFixed(2)}`,maxBase.x,maxBase.y);
		}
		//circle if enemy found
		if(found[out]){
				context.beginPath();
				context.strokeStyle = 'red';
				context.arc(unitArray[out].posVector.x + unitArray[out].velVector.x + 5,unitArray[out].posVector.y + unitArray[out].velVector.y + 5,10,0, 2*Math.PI);
				context.stroke();
		}
		//write Z height
		context.font = '12px monospace';
		context.fillStyle = `black`;
		context.fillText(`+${unitArray[out].posVector.z.toFixed(2)}`,unitArray[out].posVector.x + 11,unitArray[out].posVector.y + 11);
	}
	const buffer = canvas.toBuffer('image/png');
	fs.writeFileSync(`./coneTest/image${i}.png`,buffer);
}
*/