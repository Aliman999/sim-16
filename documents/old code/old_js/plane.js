'use strict'
const MAX_MOVE_DISTANCE = 5;
const MAX_VEL_CHANGE = 5;
import { Unit } from '/js/game/unit.js';
import * as THREE from '../../build/three.module.js';
class Plane extends Unit{
	range = 200;
	scanAngle = 10;
	heading = 0;
	scanHeading = 0;
	//constructor for a plane
	constructor(name, x_pos, y_pos, z_pos){
		super(name, x_pos, y_pos, z_pos);
	}
	
	moveWithVelocity(maxSize){
		let x_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		let y_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		let z_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		
		this.velVector.x += x_vel;
		this.velVector.y += y_vel;
		this.velVector.z += z_vel;
		
		if(Math.abs(this.velVector.x) > MAX_MOVE_DISTANCE){
			this.velVector.x = (this.velVector.x / Math.abs(this.velVector.x)) * MAX_MOVE_DISTANCE;
		}
		if(Math.abs(this.velVector.y) > MAX_MOVE_DISTANCE){
			this.velVector.y = (this.velVector.y / Math.abs(this.velVector.y)) * MAX_MOVE_DISTANCE;
		}
		if(Math.abs(this.velVector.z) > MAX_MOVE_DISTANCE){
			this.velVector.z = (this.velVector.z / Math.abs(this.velVector.z)) * MAX_MOVE_DISTANCE;
		}
		
		this.posVector.add(this.velVector);
		
		if(this.posVector.x >= maxSize){
			this.posVector.x = maxSize - 25;
			this.velVector.x *= -1;
		}
		else if(this.posVector.x < 0){
			this.posVector.x = 25;
			this.velVector.x *= -1;
		}

		
		if(this.posVector.y >= maxSize){
			this.posVector.y = maxSize - 25;
			this.velVector.y *= -1;
		}
		else if(this.posVector.y < 0){
			this.posVector.y = 25;
			this.velVector.y *= -1;
		}

		
		if(this.posVector.z >= maxSize){
			this.posVector.z = maxSize - 25;
			this.velVector.z *= -1;
		}
		else if(this.posVector.z < 0){
			this.posVector.z = 25;
			this.velVector.z *= -1;
		}

	}

	run() {
		if(this.target.length > 0){
			this.heading = this.target.heading;
			this.velVector = this.goToTarget(this.target[0].posVector, 0.4, 30, 10, this.config.FIELD_SIZE);
			this.performMovement();
		}
	}
}


export { Plane };