'use strict'
import * as THREE from '../../build/three.module.js';
function normalizeVector(vec, refBase){
	let normalizedVec = new THREE.Vector3(0, 0, 0);
	normalizedVec.x = (vec.x - refBase.x) / Math.abs(vec.x - refBase.x);
	normalizedVec.y = (vec.y - refBase.y) / Math.abs(vec.y - refBase.y);
	normalizedVec.z = (vec.z - refBase.z) / Math.abs(vec.z - refBase.z);
	if(isNaN(normalizedVec.x))
	normalizedVec.x = 0;
	if(isNaN(normalizedVec.y))
	normalizedVec.y = 0;
	if(isNaN(normalizedVec.z))
	normalizedVec.z = 0;
	return normalizedVec;
}

function vectorLength(vec, startingPos){
	return Math.sqrt(Math.pow((vec.x - startingPos.x), 2) + Math.pow(vec.y - startingPos.y, 2) + Math.pow(vec.z - startingPos.z, 2));
}

function unitVector(vec, refBase){
	let unitVec = new THREE.Vector3(0, 0, 0);
	let vecLength = vectorLength(vec, refBase);
	unitVec.x = (vec.x - refBase.x) / vecLength;
	unitVec.y = (vec.y - refBase.y) / vecLength;
	unitVec.z = (vec.z - refBase.z) / vecLength;
	return unitVec;
}

function dotProduct(vec1, vec2){
	let dot = vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
	return dot;
}

//control speed for movement functions
function boundPos(posVector, movementMod, bounds, fieldSize) {
	let modVec = new THREE.Vector3(0, 0, 0);
	if (posVector.x > fieldSize - bounds) {
		modVec.x = -movementMod;
	}
	if (posVector.x < bounds) {
		modVec.x = movementMod;
	}
	if (posVector.y > fieldSize - bounds) {
		modVec.y = -movementMod;
	}
	if (posVector.y < bounds) {
		modVec.y = movementMod;
	}
	if (posVector.z > fieldSize - bounds) {
		modVec.z = -movementMod;
	}
	if (posVector.z < bounds) {
		modVec.z = movementMod;
	}
	return modVec;
}

function boundSpeed(velVector, maxSpeed) {
	if (Math.abs(velVector.x) > maxSpeed) {
		velVector.x = (velVector.x / Math.abs(velVector.x)) * maxSpeed;
	}
	if(isNaN(velVector.x)){
		velVector.x = 0;
	}
	if (Math.abs(velVector.y) > maxSpeed) {
		velVector.y = (velVector.y / Math.abs(velVector.y)) * maxSpeed;
	}
	if(isNaN(velVector.y)){
		velVector.y = 0;
	}
	if (Math.abs(velVector.z) > maxSpeed) {
		velVector.z = (velVector.z / Math.abs(velVector.z)) * maxSpeed;
	}
	if(isNaN(velVector.z)){
		velVector.z = 0;
	}
	return velVector;
}

function vectorEquality(vec1, vec2){
	if(vec1.x != vec2.x)
		return false;
	if(vec1.y != vec2.y)
		return false;
	if(vec1.z != vec2.z)
		return false;
	return true;
}

export {
	normalizeVector,
	vectorLength,
	unitVector,
	dotProduct,
	boundSpeed,
	boundPos,
	vectorEquality
};