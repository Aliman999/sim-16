'use strict'
import * as CONFIG from '/js/game/config.js';
import * as THREE from '../../build/three.module.js';

import { random } from '/js/game/random.js';
import { getModel } from '/js/index.js';
import { Plane } from '/js/game/plane.js';
import { Center } from '/js/game/center.js';
import { Radar } from '/js/game/radar.js';
import { Missile } from '/js/game/missile.js';
import { Vehicle } from '/js/game/vehicle.js';

class Theater {
    numRed = 0;
    numBlu = 0;

    constructor(numBlu, bluDirection = "north", numRed, redDirection = "south"){
        this.numRed = numRed;
        this.numBlu = numBlu;

        this.blufor.parent = this;
        this.blufor.direction = bluDirection;

        this.redfor.parent = this;
        this.redfor.direction = redDirection;
    }

    init = async function () {
        await this.redfor.spawnPlane(this.numRed);
    }

    blufor = {
        parent:null,
        direction:null,
        spawnPlane: async function (num) {
            for (var x = 0; x < num; x++) {
                var position = spawnLocation(this.direction);
                let position_x = position.position_x;
                let position_y = 0;
                let position_z = position.position_z;
                const model = "f_16";
                await getModel(model).then((model) => {
                    const group = new THREE.Group();
                    const obj = new Plane("Blufor Plane - " + x, position_x, position_y, position_z);
                    group.add(model);
                    group.name = obj.id;
                    group.position.copy(obj.posVector);
                    obj.model = group;
                    obj.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
                    obj.link = this.link;
                    this.planes.push(obj);
                    this.parent.unitCategory.air.push(obj);
                    this.parent.units.push(obj);
                });
            }
        },
        spawnCenter: async function (num) {
            for (var x = 0; x < num; x++) {
                var position = spawnLocation(this.direction);
                let position_x = position.position_x;
                let position_z = position.position_y;
                const model = "center";
                await getModel(model).then((model) => {
                    const group = new THREE.Group();
                    const obj = new Center("Center - " + x, position_x, 0, position_z);
                    group.add(model);
                    group.name = obj.id;
                    group.position.copy(obj.posVector);
                    obj.model = group;
                    obj.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
                    obj.link = this.link;
                    this.centers.push(obj);
                    this.parent.unitCategory.ground.push(obj);
                    this.parent.units.push(obj);
                });
            }
        },
        spawnRadar: async function (num) {
            for (var x = 0; x < num; x++) {
                var position = spawnLocation(this.direction);
                let position_x = position.position_x;
                let position_z = position.position_y;
                const model = "radar";
                await getModel(model).then((model) => {
                    const group = new THREE.Group();
                    const obj = new Radar("Radar - " + x, position_x, 0, position_z, model);
                    group.add(model);
                    group.name = obj.id;
                    group.position.copy(obj.posVector);
                    obj.model = group;
                    obj.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
                    obj.link = this.link;
                    this.radars.push(obj);
                    this.parent.unitCategory.ground.push(obj);
                    this.parent.units.push(obj);
                });
            }
        },
        spawnVehicle: async function (num) {
            for (var x = 0; x < num; x++) {
                var position = spawnLocation(this.direction);
                let position_x = position.position_x;
                let position_z = position.position_y;
                const model = "vehicle";
                await getModel(model).then((model) => {
                    const group = new THREE.Group();
                    const obj = new Vehicle("Vehicle - " + x, position_x, 0, position_z, model);
                    group.add(model);
                    group.name = obj.id;
                    group.position.copy(obj.posVector);
                    obj.model = group;
                    obj.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
                    obj.link = this.link;
                    this.vehicles.push(obj);
                    this.parent.unitCategory.ground.push(obj);
                    this.parent.units.push(obj);
                });
            }
        },
        link: new Event("Link"), // Link-16 simple simulation
        planes: [],
        vehicles: [],
        centers: [],
        radars: []
    }

    redfor = {
        parent: null,
        direction: null,
        spawnPlane: async function (num) {
            for (var x = 0; x < num; x++) {
                var position = spawnLocation(this.direction);
                let position_x = position.position_x;
                let position_y = position.position_y;
                let position_z = position.position_z;
                const model = "f_16";
                await getModel(model).then((model) => {
                    const group = new THREE.Group();
                    const obj = new Plane("Blufor Plane - " + x, position_x, position_y, position_z);
                    group.add(model);
                    group.name = obj.id;
                    group.position.copy(obj.posVector);
                    obj.model = group;
                    obj.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
                    obj.link = this.link;
                    this.planes.push(obj);
                    this.parent.unitCategory.air.push(obj);
                    this.parent.units.push(obj);
                });
            }
        },
        link: new Event("Link2"), // Redfor version of Link-16 simple simulation
        planes: [],
        vehicles: [],
        centers: [],
        radars: []
    }

    calculateDistance = function (obj, obj2) {
        return Math.sqrt(
            Math.pow((obj.posVector.x - obj2.posVector.x), 2) +
            Math.pow((obj.posVector.y - obj2.posVector.y), 2) +
            Math.pow((obj.posVector.z - obj2.posVector.z), 2)
        );
    }

    checkScan = async function (obj) {
        var units = this.unitCategory;
        if (obj instanceof Radar) {
            for (let i = 0; i < units.air.length; i++) {

                // Get possible target heading information
                var unitHeading = obj.calculateHeading(units.air[i]);
                var unitDistance = obj.calculateDistance(units.air[i]);

                var posVector = units.air[i].posVector;

                /* Waiting for systems to develop further before implementing this.
                var chance = 1 - (unitDistance / obj.range);
                var randomNum = -Math.pow(Math.random(), 3) + 1;

                Checks if radar face is ahead of the possible target by 5 degrees and within distance, Locks target.
                Radar transmits lock information on its active radio frequency.
                Recieving vehicles will decide to fire on information fed to them using link event.
                */
                if (obj.targetInCone(obj.posVector, obj.scanAngle, units.air[i], obj.heading, obj.range, 200)) {
                    obj.lock(units.air[i].id, posVector, unitHeading, unitDistance);
                    return true;
                }
            }
        } else if (obj instanceof Missile) {
            if (obj.type === "AA") {
                for (let i = 0; i < units.air.length; i++) {

                    // Get possible target heading information
                    var unitHeading = obj.calculateHeading(units.air[i]);
                    var unitDistance = obj.calculateDistance(units.air[i]);

                    var posVector = units.air[i].posVector;

                    /* Waiting for systems to develop further before implementing this.
                    var chance = 1 - (unitDistance / obj.range);
                    var randomNum = -Math.pow(Math.random(), 3) + 1;
                    */

                    // Null safe operator works even if there is no target information.
                    if (obj.target[0]?.id == units.air[i].id) {
                        obj.lock(units.air[i].id, posVector, unitHeading, unitDistance);
                        return true;
                    }
                }
                /*
                If the loop completes it returns false the missile is killed meaning the missile lost its lock.
                This is bound to change as we want missiles to keep moving past the target with inertia and fall to the ground and impact the ground destroying the object.
                */
                return false;
            } else if (obj.type === "AG") {
                //This is for air to ground missiles
            }
        }
    }

    calculateDistance = function (obj, dest) {
        return Math.sqrt(
            Math.pow((dest.posVector.x - obj.posVector.x), 2) +
            Math.pow((dest.posVector.y - obj.posVector.y), 2) +
            Math.pow((dest.posVector.z - obj.posVector.z), 2)
        );
    }


    kill = function (obj) {
        new Promise(callback =>{
            /*
            This function uses a promise to maintain the order of incoming indexes.
            I came across issues with out the promise and objects passes were not removing the correct items because its very complicated
            I will explain if you ask about it.
            */
            obj.isAlive = false;

            let index = this.units.indexOf(obj);
            this.units.splice(index, 1);
            if(obj instanceof Plane){
                index = this.unitCategory.air.indexOf(obj);
                this.unitCategory.air.splice(index, 1);
            }else if(obj instanceof Vehicle){
                index = this.unitCategory.ground.indexOf(obj);
                this.unitCategory.ground.splice(index, 1);
            }else if(obj instanceof Missile){
                index = this.unitCategory.weapon.indexOf(obj);
                this.unitCategory.weapon.splice(index, 1);
            }else if(false){ //Ships not implemented
                index = this.unitCategory.sea.indexOf(obj);
                this.unitCategory.sea.splice(index, 1);
            }

            callback();
        })
    }

    checkKill = async function (location) {
        /*
        Location is passed instead of the object because of very complicated reference problems.

        Explanation:
        Location is passed because of the reference because if this function is true then this function passes into another function this.kill(object);
        If the passes object is the parameter of this function then nodejs will not allow the function within the function to delete the reference of the reference including the splice or nullifying the reference of the reference object.
        If the full object is passed as a parameter this.kill(obj);
        It will not splice the object out of the arrays of units and the object will not nullify or delete and will keep rendering in the canvas.
        Thus the object because not nullfied will lead to a memory leak because of non garbage collected objects.
        This references references of references ...
        */
        var units = this.units;
        for (var i = 0; i < units.length; i++) {
            var unitDistance = this.calculateDistance(location, units[i]);
            if (unitDistance < CONFIG.KILL_DISTANCE) {
                await this.kill(units[i]);
            }
        }
    }

    randomLock = function(obj){
        /*
        This was a temporary function created to assign planes to fly towards blufor ground units in order for the blufor radar to shoot at them. and create other unitTests.
        */
        var target = this.unitCategory.ground[random(this.unitCategory.ground.length)];
        var heading = obj.calculateHeading(target);
        var distance = obj.calculateDistance(target);
        obj.lock(target.id, target.posVector, heading, distance);
    }
    //This makes it easy to reference specific object and object types
    unitCategory = {
        ground: [],
        air: [],
        weapon: [], // This is for weapons that are large enough to be shot down by defence weapons such as ships Phalanx CIWS and Iron Dome
        sea: []
    }
    //Full list of the active units on the field. Everything in here is rendered on canvas and passed to web socket.
    units = []
}

function ranRange(min, max) { // Random range function created to create spawn locations in sections of the map.
    return (Math.random() * (max - min + 1)) + min
}

/*
This function was created in order to keep units closer together instead of by chance being close to one another.
You pass in the function a parameter a compass direction and it will return coordinates in that general area.
*/
function spawnLocation(direction){
    var cohersion = 3; // Do not change
    var boundaries = 0; //This will push spawned units away from the boundaries of the map.
    var directions = {
        north: function () {
            return {
                position_x: ranRange(CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion),
                position_y: ranRange(boundaries, CONFIG.FIELD_SIZE / (cohersion)),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        northeast: function () {
            return {
                position_x: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_y: ranRange(boundaries, CONFIG.FIELD_SIZE / cohersion),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        east: function () {
            return {
                position_x: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_y: ranRange(CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion ),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        southeast: function () {
            return {
                position_x: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_y: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        south: function () {
            return {
                position_x: ranRange(CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion),
                position_y: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        southwest: function () {
            return {
                position_x: ranRange(boundaries, CONFIG.FIELD_SIZE / cohersion),
                position_y: ranRange(CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        west: function () {
            return {
                position_x: ranRange(boundaries, CONFIG.FIELD_SIZE / cohersion),
                position_y: ranRange(CONFIG.FIELD_SIZE / cohersion, CONFIG.FIELD_SIZE - CONFIG.FIELD_SIZE / cohersion),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        },
        northwest: function () {
            return {
                position_x: ranRange(boundaries, CONFIG.FIELD_SIZE / cohersion),
                position_y: ranRange(boundaries, CONFIG.FIELD_SIZE / cohersion),
                position_z: random(CONFIG.FIELD_SIZE)
            }
        }
    }
    return directions[direction]();
}

export { Theater };