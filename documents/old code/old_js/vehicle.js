'use strict'
import { Unit } from '/js/game/unit.js';
import { Missile } from '/js/game/missile.js';
import { random } from '/js/game/random.js';
import { getModel } from '/js/index.js';
class Vehicle extends Unit {
    range = 0; // Range is determined by the weapons on the vehicle.
    weapons = [];
    feeding = [];
    loaded = null;

    interval = 60; // Set to 60 because when the vehicle gets a target it will fire immediately.

    lockAttempt = 0; // Used to clean up target locks. While running, if the vehicle is not fed a target in x frames it will drop lock.

    heading = random(360);

    mode = "standby";
    power = true;

    //In the future the model will decide what types of missiles its using and the missiles will determine the vehicles range etc.
    constructor(name, x, y, z) {
        super(name, x, y, z);
        this.scanAngle = 20;
    }

    // Passes target information sourced from a radar or center onto the missile recently fired. This will stop feeding the missile information if the missile has a radar seeker of its own.
    feed = function(){
        for(var i = 0; i < this.feeding.length; i++){
            var obj = this.feeding[i];
            // The missile will change modes when fired to active meaning its flying and burning fuel guided by the vehicle.
            for(var x = 0; x < this.target?.length; x++){
                if(obj.target[0].id === this.target[x].id){
                    var target = this.target[x];
                    if (obj.mode == "active") {
                        var heading = obj.calculateHeading(target);
                        var distance = obj.calculateDistance(target);
                        obj.lock(target.id, target.posVector, heading, distance);
                    } else {
                        //If the missile dies or goes 'pitbull' aka seeking out the target with its own radar then the vehicle stops feeding data to the missile.
                        let index = this.feeding.indexOf(obj);
                        this.feeding.splice(index, 1);
                    }
                }
            }
        }
    }

    /* 
    This loads the missile with the fire event listener and gives the missile a lock.
    It will then tell the missile to fire and this vehicle will tell the theater that it fired and pushes the fired weapon to the theater to theater.units to render.
    The event listner in theater will pass the object this.events.emit("fire", ->this.loaded<-); to the theater.
    */

    fireEvent = new CustomEvent('fire', this.loaded);

    fire = function (target) {
        if (this.loaded) {
            /*
            this.loaded.events.once("fire", () => {
                this.events.emit("fire", this.loaded);
            });

            this.loaded.lock(target.id, target.posVector, target.heading, target.distance);
            this.loaded.fire();
            this.loaded.events.off("fire", () => { });
            this.feeding.push(this.loaded);

            this.weapons.shift();
            if (this.weapons.length > 0) {
                this.loaded = this.weapons[0];
            }
            
            return true;
            */
        } else {
            this.mode = "reloading";
            return false;
        }
    }

    /*
    Vehicles runtime

    The first frame of the vehicle sets up weapons and loades a weapon. I'm not exactly sure why i chose this approach. I feel as though the if statement this.setup === true is a waste of time.
    */
    run = async function () {
        if (this.setup === true) {
            this.interval++;
            if (this.target.length > 0) {
                /*
                Every frame the vehicle checks if it was fed target information. If the target information was given then it face the missiles towards the target.
                This was in an attempt to get smooth vector lines to the target but it doesnt work but we're keeping it for when the face of the missile matters.
                */
                this.mode = "active";

                //This is a redundant safe feeding function to ensure that missiles fired by this vehicle are constantly fed updated target information.
                //this.feed();

                // Vectors for canvas drawing to show target marking
				/*
                this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * this.target.distance;
                this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * this.target.distance;
				*/
                /*
                Every time run is called this.interval++ is the delay for the next missile to be loaded and fired.
                This vehicle will only reload when it's active missile is either dead or 'pitbull' aka seeking its own target with its independent radar.
                */
                if (this.loaded) {
                    this.loaded.heading = this.target.heading;
                    this.target.forEach(target => {
                        if (target.distance < this.loaded.range && this.interval >= 60) {
                            this.interval = 0;
                            this.fire(target);
                        }
                    })
                }
                
                //Cleans up lock incase it is not fed new target information.
                this.target.forEach(element => {
                    if (element.lockAttempt > this.config.LOCK_LATENCY) {
                        this.unlock(element);
                        element.lockAttempt = 0;
                    } else {
                        element.lockAttempt++;
                    }
                });
            }else{
					 /*
                this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * 0;
                this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * 0;
					 */
                this.mode = "standby";
            }
        } else {
            this.setup = true;
            //Setup
            for (var x = 0; x < 6; x++) {
                const model = "aim_120_d";
                var obj = new Missile("AIM 120 D - " + x, this.posVector.x, this.posVector.y, this.posVector.z, "AIM120D");
                this.range = obj.range;
                await getModel(model).then((model) => {
                    model.name = obj.id;
                    obj.model = model;
                    this.model.add(model);
                    this.weapons.push(obj);
                });
            }
            this.loaded = this.weapons[0];
			/*
            this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * 0;
            this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * 0;
			*/
        }
    }
}

export { Vehicle };