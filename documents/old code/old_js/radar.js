'use strict'
import { Unit } from '/js/game/unit.js';
import { random } from '/js/game/random.js';
import * as THREE from '../../build/three.module.js';
class Radar extends Unit{
    model = "";
    range = 600;
	scanAngle = 65;
    heading = random(360);
	
	lockAttempt = 0;
    mode = "scan";
    power = true;


    //Modes:
    //Scan - Wide Frequency Scanning
    //Track - Target Aquired and Tracking Target AKA Spiking the aircraft
    constructor(name, x, y, z){
		super(name, x, y, z);
    }

    linkTransmit(){ // Sends target information to other vehicles on the same frequency within range of the radar array.
        for(var i = 0; i < this.radio.length; i++){
            //this.link.emit(this.radio[i], this);
        }
    }
    
    run = function(){
        if (this.target.length > 0) {
            // Draw radar line of sight
            /*
			this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * this.target.distance;
            this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * this.target.distance;
            */
            //this.linkTransmit();

            this.targetCenterPoint();

            if (this.lockAttempt > 3) { // Clear the locked target to check if the object is still getting new target data. If false then the radar will return to searching for a target.
                this.unlock();
                this.lockAttempt = 0;
            } else {
                this.lockAttempt++;
            }

        }else{
            // Rotates radar dish.
            const unit = this.model.children[0];
            const cone = this.model.children[1];

            var headVector = this.getBaseFromHeading(300);

            //This block of code represents what the object is locked on to.
            var rotationMatrix = new THREE.Matrix4();
            var targetQuaternion = new THREE.Quaternion();
            //console.log(headVector);
            rotationMatrix.lookAt(headVector, this.model.position, this.model.up);
            targetQuaternion.setFromRotationMatrix(rotationMatrix);
            cone.quaternion.rotateTowards(targetQuaternion, Math.PI / 2);

            this.heading += Math.PI / 4;
            if (this.heading > 360) {
                this.heading = 0;
            }
            // Draw radar line of sight
			/*
            this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * this.range;
            this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * this.range;
			*/
		}
    }

    enable = function(){
        this.active = true;
    }

    disable = function(){
        this.active = false;
    }
    
}

export { Radar }