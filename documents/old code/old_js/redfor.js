'use strict'

function random(max) {
    return Math.floor(Math.random() * max);
}

var names = [
    "Yemelyan",
    "Koldan",
    "Vitaliy",
    "Georgiy",
    "Adrik",
    "Apostol",
    "Aleksei",
    "Roman",
    "Vanya",
    "Sevastian",
    "Serafim",
    "Ioakim",
    "Matvei",
    "Feliks",
    "Ivan",
    "Misha",
    "Toma",
    "Yevgeniy",
    "Stas",
    "Borislav",
    "Toma",
    "Stefan",
    "Mikhail"
];

var planes = [
    "Sukhoi Su-24",
    "Sukhoi Su-25",
    "Sukhoi Su-27",
    "Sukhoi Su-30",
    "Sukhoi Su-34",
    "Mikoyan MiG-29",
    "Mikoyan MiG-31",
    "Mikoyan MiG-35",
    "Mikoyan MiG-25"
];

class redfor{}

function redfor() {
    var name = names[random(names.length)];
    var plane = planes[random(planes.length)];
    var x_pos = 0;
    var y_pos = 0;
    var z_pos = 0;
    //console.log("Spawned " + plane + " - " + name);
    return {name, plane, alive:true};
}

module.exports = redfor;
