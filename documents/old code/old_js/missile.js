'use strict'
import { Unit } from '/js/game/unit.js';
import * as vector from '/js/game/vector.js';
class Missile extends Unit {
    mode = null;
    power = true;
	 
    //TYPES: AA | AG
    type = "AA";

    range = 0;
	scanAngle = 10;
    speed = 0;
    fuel = 100;
    feed = false;
    battery = 0;
    name = "";

    AIM120D = function(){
        this.type = "AA";
        this.range = 500;
        this.speed = 4;
        this.name = "AIM-120 AMRAAM"
        this.radar = "active";
    }

    goToTarget(targetPosition, maxSpeed, moveMod, bounds, fieldSize) {
        //Custom velocity vector function different from parent Units
        let cohVector = vector.subVector(targetPosition, this.posVector);
        let modVelVector = this.velVector;
		
		/*
			we want the missile to rise to correct Z before closing in on target
			we do this by scaling the horizontal movement by the diff in altitude
		*/
		
		//alt diff
		let altDiff = this.posVector.z / targetPosition.z;
        //forces missile to close in altitude
        if (Math.abs(cohVector.x) > maxSpeed) {
            cohVector.x = (cohVector.x / Math.abs(cohVector.x)) * maxSpeed;
			cohVector.x = cohVector.x * altDiff;
        }
        if (Math.abs(cohVector.y) > maxSpeed) {
            cohVector.y = (cohVector.y / Math.abs(cohVector.y)) * maxSpeed;
			cohVector.y = cohVector.y * altDiff;
        }
        if (Math.abs(cohVector.z) > maxSpeed) {
            cohVector.z = (cohVector.z / Math.abs(cohVector.z)) * maxSpeed;
        }
        
        //adds to velocity vector
        modVelVector = vector.addVector(modVelVector, cohVector);
		//dampen movement as we approach the target
		modVelVector.x *= Math.abs((targetPosition.x - this.posVector.x) / 1);
		modVelVector.y *= Math.abs((targetPosition.y - this.posVector.y) / 1);
		modVelVector.z *= Math.abs((targetPosition.z - this.posVector.z) / 1);
        //checks if plane going out of bounds
        modVelVector = vector.addVector(modVelVector, vector.boundPos(this.posVector, moveMod, bounds, fieldSize));
        //binds the speed to a specific limit
        modVelVector = vector.boundSpeed(modVelVector, maxSpeed);
        return modVelVector;
    }

    // Constructor for a missile
    constructor(name, x_pos, z_pos, y_pos, model) {
        super(name, x_pos, z_pos, y_pos);
        
        this.name = name;
        this[model](); // Runs initialization function after the name of the model eg. NASAMS
        this.mode = "standby";
    }

    addNewConnection(plane) {
        let planeObject = { 'object': plane, 'distance': this.calculateDistance(plane) };
        this.link_connections.push(planeObject);
    }

    calculateDistance(plane) {
        return Math.sqrt(
            Math.pow((plane.posVector.x - this.posVector.x), 2) +
            Math.pow((plane.posVector.y - this.posVector.y), 2) +
            Math.pow((plane.posVector.z - this.posVector.z), 2)
        );
    }

    fire = new Event('fire');
    explode = new CustomEvent('explode', this);

    fire() { // Missile gets pushed to theater.units and becomes an object in the simulation.
        this.dispatchEvent(fire);
        this.mode === "active";
    }

    explode() { // Missile explodes and theater checks if anything is within kill range.
        if (this.power) {
            this.power = false;
            this.dispatchEvent(explode);
        }
    }

    run() { // Missile runtime 
        if(this.fuel === 0){
            //missile keeps moving with inertia
            //this.explode();
        }else if(this.battery === 0){
            //Missile stops tracking but keeps moving
            //this.explode();
        }
        if (this.target.length > 0) {
            this.battery -= 0.2;
            this.fuel -= 0.4;

            if (this.radar = "active") { //Missile is half guided
                this.heading = this.target.heading;

                this.velVector = this.goToTarget(this.target[0].posVector, this.speed, 30, 10, this.config.FIELD_SIZE);
                this.performMovement();


                if(this.mode == "pitbull"){ // Missile is no longer recieving target information from vehicle and is seeking the target on its own.
                    this.battery--;
                } else if (this.target.distance < this.battery){ // Missile is within range of the target and has enough battery to reach the target.
                    this.mode = "pitbull";
                }

            } else if (this.radar = "semi") {
                //100% guideance
            } else {
                //IR Guided
            }

            if (this.target[0].distance <= this.config.KILL_DISTANCE) { // If you dont understand this god help you.
                this.explode();
            }

            if (this.target[0].lockAttempt > 10) { // Clear the locked target to check if the object is still getting new target data. If false then the missile will fall to the ground.
                this.unlock(this.target[0]);
            } else {
                this.target[0].lockAttempt++;
            }
        } else {
            console.log(this.name+"LOST LOCK");
            this.explode();
        }
    }
}

export { Missile };