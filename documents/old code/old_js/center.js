'use strict'
import { Unit } from '/js/game/unit.js';
class Center extends Unit{
    constructor(name, x, y, model) {
		 super(name, x, y, 0);
         this.model = model;
    }

    preset = function () {
        if (typeof (freq) == "number" && typeof (band) == "string") {
            this.link_connections.push({ freq, band, active:false});
        } else {
            return "Invalid Input: Freq typeof Number, Band typeof String, Active typeof Boolean";
        }
    }

    //preset Index
    enable = function (index) {
        try {
            this.link_connections[index].active = true;
            return "Enabled " + this.link_connections[index].freq + this.link_connections[index].band;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    disable = function(index){
        try {
            this.link_connections[index].active = false;
            return "Disabled "+this.link_connections[index].freq + this.link_connections[index].band;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    connect = function(obj){
        var name = obj.name;
        var distance = Math.sqrt(
            Math.pow((obj.posVector.x - this.posVector.x), 2) +
            Math.pow((obj.posVector.y - this.posVector.y), 2) +
            Math.pow((obj.posVector.z - this.posVector.z), 2)
        );
        this.link_connections.push({ 'object':name, 'distance':distance });
    }

    updateDistance = function(obj){
        for(var x = 0; x < this.link_connections.length; x++){
            this.link_connections[i].distance = 0;
        }
    }

    calculateDistance(obj) {
        return Math.sqrt(
            Math.pow((obj.posVector.x - this.posVector.x), 2) +
            Math.pow((obj.posVector.y - this.posVector.y), 2) +
            Math.pow((obj.posVector.z - this.posVector.z), 2)
        );
    }

    updateDistance = function(){

    }

    listen = function (freq, band) {
        if (typeof (freq) == "number" && typeof (band) == "string") {
            this.link_connections.push({ freq, band, active:true });
        } else {
            return "Invalid Input: Freq typeof Number, Band typeof String";
        }
    }

    list = function(filter){
        if(filter === true){

        }else if(filter === false){

        }else{

        }
        return this.link_connections;
    }

}

export { Center };