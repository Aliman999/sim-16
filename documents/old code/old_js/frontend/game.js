'use strict'
import { Theater } from '/js/game/theater.js';
import { renderer, scene, camera, spotLight } from '/js/index.js';
import * as THREE from '../../build/three.module.js';
import * as CONFIG from '/js/game/config.js';
import * as Plane from '/js/game/plane.js';
import * as Center from '/js/game/center.js';
import * as Radar from '/js/game/radar.js';
import * as Missile from '/js/game/missile.js';
import * as Vehicle from '/js/game/vehicle.js';
import * as vector from '/js/game/vector.js';

var theater = new Theater(0, "west", 1, "east"); //# of Blu, Blu Spawn Point, # of Red, Red Spawn Point

theater.init().then(async () => {
    var blufor = theater.blufor;
    var redfor = theater.redfor;

    await blufor.spawnCenter(0);
    await blufor.spawnRadar(1);
    await blufor.spawnVehicle(1);


    /*
    for (var x = 0; x < blufor.vehicles.length; x++) { // Vehicle's fire event telling the missile to fire and pushing the missile to the theater for render.
        document.addEventListener("fire", (obj) => {
            theater.unitCategory.weapon.push(obj);
            theater.units.push(obj);
            obj.events.removeAllListeners();
            obj.events.addEventListener('explode', (ref) => {
                var location = { posVector: ref.posVector };
                theater.kill(ref);
                theater.checkKill(location);
            })
        });
    }
    */

    for (let j = 0; j < blufor.vehicles.length; j++) { // Link-16 simple version to distribute information of locked targets on a frequency
        var unit = blufor.vehicles[j];
        unit.run(); //preload vehicle with weapons
        /*
        unit.link.on(CONFIG.RADIO_FREQ.freq + CONFIG.RADIO_FREQ.band, (src) => {
            var obj = blufor.vehicles[j];
            var target = src.target;
            var posVector = target.posVector;
            var unitHeading = obj.calculateHeading(target);
            var unitDistance = obj.calculateDistance(target);
            var sourceDistance = obj.calculateDistance(src);
            if ((!obj.target || obj.lockAttempt > CONFIG.LOCK_LATENCY) && unitDistance < obj.range && sourceDistance <= CONFIG.MAX_CONNECTION_RANGE) {
                obj.lock(target.id, posVector, unitHeading, unitDistance); // Assigns vehicles targets to lock
            }
        })
        */
    }

    for (var i = 0; i < redfor.planes.length; i++) { //Sets redfor to lock random blufor units.
        var obj = redfor.planes[i];
        theater.randomLock(obj);
    }

    for (var x = 0; x < theater.units.length; x++) {
        var unit = theater.units[x];

        //View Cone
        var geometry = new THREE.ConeGeometry(unit.scanAngle * 4, unit.range, 20, true);
        geometry.translate(0, -unit.range / 2, 0);
        geometry.rotateX(-Math.PI / 2);
        var material = new THREE.MeshPhongMaterial({
            color: 0xffff00,
            opacity: 0.25,
            transparent: true,
        });
        const cone = new THREE.Mesh(geometry, material);
        unit.model.add(cone);

        scene.add(unit.model);
    }

    console.log(blufor.planes.length + " Blufor Planes");
    console.log(blufor.vehicles.length + " Blufor Sam Sites");
    console.log(blufor.radars.length + " Blufor Radars");
    console.log(blufor.centers.length + " BluFor Centers");
    console.log("----------");
    console.log(redfor.planes.length + " Redfor Planes\n");
    var maxFrames = 10;
    //Runs the scene and renders everything automatically. This function begins when called in setup.
    function animate() {
        renderer.render(scene, camera);
        spotLight.position.set(
            camera.position.x + 10,
            camera.position.y + 10,
            camera.position.z + 10
        )

        for (var x = 0; x < blufor.vehicles.length; x++) { //Execute SAM sites functions such as firing missiles/bullets and reloading when fed data from radars.
            var vehicle = blufor.vehicles[x];
            if (vehicle.power === true) {
                vehicle.run();
            }
        }

        for (var x = 0; x < blufor.radars.length; x++) { //Execute Radar functions, Theater checks if the radar is pointed at a target eg. Radar -> Plane
            var radar = blufor.radars[x];
            if (radar.power === true) {
                radar.run();
                theater.checkScan(radar);
            }
        }

        for (var x = 0; x < theater.unitCategory.weapon.length; x++) { //Execute active weapons functions eg. missiles, bullets, etc.
            var obj = theater.unitCategory.weapon[x];
            obj.run();
            if (!theater.checkScan(obj)) {
                theater.kill(obj);
            }
        }

        for (let p = 0; p < redfor.planes.length; p++) { //Execute all plane functions
            redfor.planes[p].run();
        }
        //maxFrames--;
        if(maxFrames == 0){
            throw new Error("Stop!");
        }
        requestAnimationFrame(animate);
    }
    animate();
});