'use script'
import * as THREE from '../build/three.module.js';
import { scene } from './scene.js';
import { createCamera } from './camera.js';
import { getModel } from './loadModel.js';
import './lights.js';
import { renderer } from './render.js';
import { OrbitControls } from './controls/camera/OrbitControls.js';
import { newAmbientLight, newHemisphereLight, newSpotLight, newDirectionalLight, newPointLight } from './lights.js';

//Create Camera
var camera = createCamera(75, window, 1, 2000, 500, 1000, 500);

/*
I decided to use promises because it allows a better flow of logic. 
Get the model then add it to the scene
*/
//3d Model Loader script use gtlf for small size and optimized model
//After the function gets the model it tells the renderer to render the new model into the dom.
//      (model)

//Ambient light to light up model
//                       (hex       intensity)
//scene.add(newAmbientLight(0x404040, 4));

//Hemisphere Light
//                          (hex,     hex1,     intensity)
//scene.add(newHemisphereLight(0xffeeb1, 0x080820, 4));


//Spotlight, main source of light
//                          (hex   intensity)
var spotLight = newSpotLight(0xFFA95C, 1);
spotLight.castShadow = true;
spotLight.shadow.bias = -0.0001;
spotLight.shadow.mapSize.width = 1024 * 4;
spotLight.shadow.mapSize.height = 1024 * 4;
spotLight.position.set(10, 20, 10);
scene.add(spotLight)

//Directional Lights
//                           (r    g    b    intensity)
var light = newDirectionalLight(0xffffff, 2);
light.position.set(0, 20, 0);
light.castShadow = true;
scene.add(light);

//Point Lights I believe it is just a light emitter
//                  (r    g    b    intensity)      (x  y    z)
/*
var pointLight = newPointLight(0xffffff, 10);
pointLight.position.set(0, 300, 500);
scene.add(pointLight);
pointLight.position.set(500, 100, 0);
scene.add(pointLight);
pointLight.position.set(0, 100, -500);
scene.add(pointLight);
pointLight.position.set(-500, 300, 0);
scene.add(pointLight);
*/

//Append the renderer to the webpage
document.body.appendChild(renderer.domElement);

/*
Start the camera perspective with 75 FOV, and aspect radio
The next two attributes are the near and far clipping plane.
What that means, is that objects further away from the camera than the value of far or closer than near won't be rendered. 
You don't have to worry about this now, but you may want to use other values in your apps to get better performance.

Next sets the position of the camera and tells it to look at 0, 0, 0;
*/

//Axis lines
scene.add(new THREE.AxesHelper(500));

//Creates floor 1000x1000
var geometry = new THREE.PlaneGeometry(1000, 1000, 1, 1);
var material = new THREE.MeshBasicMaterial({ color: 0xdadada });
var floor = new THREE.Mesh(geometry, material);
floor.material.side = THREE.DoubleSide;
floor.position.y = 0;
floor.position.x = 500;
floor.position.z = 500;
floor.rotation.x = Math.PI / 2;
scene.add(floor);

//Lights the scene
var spotLight = newSpotLight(0xFFA95C, 1);
spotLight.castShadow = true;
spotLight.shadow.bias = -0.0001;
spotLight.shadow.mapSize.width = 1024 * 4;
spotLight.shadow.mapSize.height = 1024 * 4;
spotLight.position.set(10, 20, 10);
scene.add(spotLight);

//Confirms that objects are loaded and finished.
var load = false;
var loadFinish = false;

//Holds initilialized models to be copied and used in the scene.
var models = {
    
}
var unitData = [];

socket.ws.onmessage = function (data) {
    data = JSON.parse(data.data);
    data = data.data;
    if (load === false) {
        setup(data);
    } else {
        if(loadFinish === true){
            theater(data);
        }
    }
};

async function setup(data) {
    load = true;
    for (var x = 0; x < data.length; x++) {
        var unit = data[x];
        await getModel(unit).then((model, err) => {
            if (err) throw new error;
            model.traverse(n => {
                if (n.isMesh) {
                    n.castShadow = true;
                    n.recieveShadow = true;
                    if (n.material.map) n.material.map.anisotropy = 16;
                }
            })
            models[unit] = model;
        });
    }
    loadFinish = true;
    animate();
}

function run(item){
    const group = scene.getObjectByName(item.id);
    const unit = group.children[0];
    const cone = group.children[1];
    var headVector = getBaseFromHeading(item, 100);

    if (item.target.length > 0) {
        for(var i = 0; i < item.target.length; i++){
            try{
                const target = scene.getObjectByName(item.target[i].id);
                const sphere = target.children[2];
                var relativeVector = new THREE.Vector3().copy(target.position);
                relativeVector.sub(group.position);
                headVector.copy(target.position);

                sphere.material.opacity = 0.25;
            }catch(e){

            }
        }
    } else {
        
    }

    //Makes the object face the velocity of the object
    var velVector = new THREE.Vector3(item.velVector.x, item.velVector.z, item.velVector.y);
    var posVector = new THREE.Vector3(item.posVector.x, item.posVector.z, item.posVector.y);
    // Creates the modified vector that determines where the object is going to face.
    var modVector = posVector.add(velVector);
    //Weird Quaternion stuff
    var velMatrix = new THREE.Matrix4();
    var velQuaternion = new THREE.Quaternion();
    velMatrix.lookAt(modVector, group.position, group.up);
    //Creates the amount to rotate given the matrix
    velQuaternion.setFromRotationMatrix(velMatrix);
    //Makes the unit rotate towards the given Quaternion
    unit.quaternion.rotateTowards(velQuaternion, Math.PI / 2);

    //This block of code represents what the object is locked on to.
    var rotationMatrix = new THREE.Matrix4();
    var targetQuaternion = new THREE.Quaternion();
    //console.log(headVector);
    rotationMatrix.lookAt(headVector, group.position, group.up);
    targetQuaternion.setFromRotationMatrix(rotationMatrix);
    cone.quaternion.rotateTowards(targetQuaternion, Math.PI / 2);
}

function move(item){
    const group = scene.getObjectByName(item.id);
    const unit = group.children[0];
    if (item.velVector) {
        const step = 0.1;
        //console.log(item.velVector); p = a + (b - a) * t
        group.translateZ(step);
        //unit.translateZ();
    }
}

//Controls when to add units to the scene and when to remove them
function theater(units) {
    function comparer(otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                return other.id == current.id
            }).length == 0;
        }
    }

    if(units.length > unitData.length){
        var add = units.filter(comparer(unitData));
        add.forEach((item, i) => {
            var model = models[item.model].clone();
            model.name = item.id;

            //View Cone
            var geometry = new THREE.ConeGeometry(item.scanAngle*4, item.range, 20, true);
            geometry.translate(0, -item.range / 2, 0);
            geometry.rotateX(-Math.PI / 2);
            var material = new THREE.MeshPhongMaterial({
                color: 0xffff00,
                opacity: 0.25,
                transparent: true,
            });
            const cone = new THREE.Mesh(geometry, material);

            //Target Sphere
            geometry = new THREE.SphereGeometry(15, 32, 16);
            material = new THREE.MeshPhongMaterial({
                color: 0xff0000,
                opacity: 0,
                transparent: true,
            });
            const sphere = new THREE.Mesh(geometry, material);

            //Group of objects
            const group = new THREE.Group();
            group.name = item.id;
            group.position.set(item.posVector.x, item.posVector.z, item.posVector.y);

            group.add(model);
            group.add(cone);
            group.add(sphere);

            //console.log({group, item});
            scene.add(group);
            unitData.push(item);
        })
    }else if(units.length < unitData.length){
        var remove = unitData.filter(comparer(units));
        remove.forEach((item, i) => {
            var selectedObject = scene.getObjectByName(item.id);
            scene.remove(selectedObject);
            unitData.splice(unitData.indexOf(item), 1);
        })
    }else{
        unitData = units;
    }
    return;
}


//radar functions
function getBaseFromHeading(obj, altitude){
    /* 
        the goal is to calculate a horizontal base to the unit such that it points in the direction of the heading
        we can therefore ignore the z axis, setting it to 0 for calculation purposes
    */
    //we start with a generic vector on the unit circle pointing to 1 in the x direction
    //this is what we will rotate in order to find the direction of the heading
    let norm = new THREE.Vector3(1, 0, 0);
    //convert degrees to rads
    let rads = obj.heading * (Math.PI / 180);
    //we rotate our normal on our modified heading
    let shiftX = norm.x * Math.cos(rads);
    let shiftY = norm.x * Math.sin(rads);
    norm.x = shiftX;
    norm.z = shiftY;
    //we now extend out our base using the length of our radar scan
    norm.multiply(new THREE.Vector3(obj.range, obj.range, obj.range));
    //finally we add this new base to our current position to get it relative to the unit
    let baseVector = new THREE.Vector3(obj.posVector.x, obj.posVector.z, obj.posVector.y);
    baseVector.add(norm);
    baseVector.y = altitude;
    return baseVector;
}

//Orbit camera
var controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(500, 0, 500);
camera.lookAt(500, 0, 500);
//controls.addEventListener('change', animate);

//Runs the scene and renders everything automatically. This function begins when called in setup.
var bool = true;
function animate() {
    renderer.render(scene, camera);
    spotLight.position.set(
        camera.position.x + 10,
        camera.position.y + 10,
        camera.position.z + 10
    )
    var len = unitData.length
    for(var x = 0; x < len; x++){
        var item = unitData[x];
        var selectedObject = scene.getObjectByName(item.id);
        if (selectedObject?.position.set(item.posVector.x, item.posVector.z, item.posVector.y)){
            move(item);
            run(item);
        } else {
            len = unitData.length;
            scene.remove(selectedObject);
        }
    }
    requestAnimationFrame(animate);
}
