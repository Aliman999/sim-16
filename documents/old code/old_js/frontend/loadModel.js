'use strict'
import { GLTFLoader } from './loaders/GLTFLoader.js';

//Takes model of the object for example F-16 points to the directory of the model
function getModel(model){
    return new Promise((callback, reject) => {
        var gltfLoader = new GLTFLoader();
        gltfLoader.load('./models/' + model + '/model.gltf', function (gltf) {
            callback(gltf.scene.children[0]);
        }, undefined, function (error) {
            reject(error);
        });
    })
}

export { getModel };