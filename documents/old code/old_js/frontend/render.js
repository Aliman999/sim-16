'use strict'
import * as THREE from './../build/three.module.js';

//Create the renderer with antialiasing
const renderer = new THREE.WebGLRenderer({alpha:true, antialias: true});
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0xffffff, 1);
//Bound to change
renderer.toneMapping = THREE.ReinhardToneMapping;
renderer.toneMappingExposure = 2.3;
renderer.shadowMap.enabled = true;

export { renderer };