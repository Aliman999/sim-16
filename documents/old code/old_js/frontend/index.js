'use script'
import * as THREE from '../build/three.module.js';
import { scene } from './scene.js';
import { createCamera } from './camera.js';
import { getModel } from './loadModel.js';
import './lights.js';
import { renderer } from './render.js';
import { OrbitControls } from './controls/camera/OrbitControls.js';
import { newAmbientLight, newHemisphereLight, newSpotLight, newDirectionalLight, newPointLight } from './lights.js';

//Create Camera
var camera = createCamera(75, window, 1, 2000, 500, 1000, 500);

/*
I decided to use promises because it allows a better flow of logic. 
Get the model then add it to the scene
*/
//3d Model Loader script use gtlf for small size and optimized model
//After the function gets the model it tells the renderer to render the new model into the dom.
//      (model)

//Ambient light to light up model
//                       (hex       intensity)
//scene.add(newAmbientLight(0x404040, 4));

//Hemisphere Light
//                          (hex,     hex1,     intensity)
//scene.add(newHemisphereLight(0xffeeb1, 0x080820, 4));


//Spotlight, main source of light
//                          (hex   intensity)
var spotLight = newSpotLight(0xFFA95C, 1);
spotLight.castShadow = true;
spotLight.shadow.bias = -0.0001;
spotLight.shadow.mapSize.width = 1024 * 4;
spotLight.shadow.mapSize.height = 1024 * 4;
spotLight.position.set(10, 20, 10);
scene.add(spotLight)

//Directional Lights
//                           (r    g    b    intensity)
var light = newDirectionalLight(0xffffff, 2);
light.position.set(0, 20, 0);
light.castShadow = true;
scene.add(light);

//Point Lights I believe it is just a light emitter
//                  (r    g    b    intensity)      (x  y    z)
/*
var pointLight = newPointLight(0xffffff, 10);
pointLight.position.set(0, 300, 500);
scene.add(pointLight);
pointLight.position.set(500, 100, 0);
scene.add(pointLight);
pointLight.position.set(0, 100, -500);
scene.add(pointLight);
pointLight.position.set(-500, 300, 0);
scene.add(pointLight);
*/

//Append the renderer to the webpage
document.body.appendChild(renderer.domElement);

/*
Start the camera perspective with 75 FOV, and aspect radio
The next two attributes are the near and far clipping plane.
What that means, is that objects further away from the camera than the value of far or closer than near won't be rendered. 
You don't have to worry about this now, but you may want to use other values in your apps to get better performance.

Next sets the position of the camera and tells it to look at 0, 0, 0;
*/

//Axis lines
scene.add(new THREE.AxesHelper(500));

//Creates floor 1000x1000
var geometry = new THREE.PlaneGeometry(1000, 1000, 1, 1);
var material = new THREE.MeshBasicMaterial({ color: 0x0000ff });
var floor = new THREE.Mesh(geometry, material);
floor.material.side = THREE.DoubleSide;
floor.rotation.x = 90;
scene.add(floor);

<<<<<<< HEAD:frontend/js/index.js
const socket = new Socket();
socket.startClient();

var models = [];
var load = false;

=======
//Lights the scene
>>>>>>> development:documents/old code/old_js/frontend/index.js
var spotLight = newSpotLight(0xFFA95C, 1);
spotLight.castShadow = true;
spotLight.shadow.bias = -0.0001;
spotLight.shadow.mapSize.width = 1024 * 4;
spotLight.shadow.mapSize.height = 1024 * 4;
spotLight.position.set(10, 20, 10);
scene.add(spotLight);

<<<<<<< HEAD:frontend/js/index.js

socket.ws.onmessage = function (data) {
    //some weirds stuff here
    data = JSON.parse(data.data);
    data = data.data;
    if (load === false && data instanceof Array) {
        setup(data);
    } else if (data instanceof Array) {
        models.forEach((item, i) => {
            item.data = data[0];
        })
    }
};

async function setup(data) {
    load = true;
    camera.lookAt(data[0].posVector.x, data[0].posVector.y, data[0].posVector.y);
    for (var x = 0; x < data.length; x++) {
        var unit = data[x];
        if (unit.name === "Redfor Plane - 0") {
            await getModel("f_16").then((model, err) => {
                if (err) throw new error;
                model.traverse(n => {
                    if (n.isMesh) {
                        n.castShadow = true;
                        n.recieveShadow = true;
                        if (n.material.map) n.material.map.anisotropy = 16;
                    }
                })

                var posVector = data[x].posVector;
                model.position.set(posVector.x, posVector.y, posVector.z);

                model.data = data[x];
                model.name = unit.name;

                models.push(model);

                scene.add(model);
            });
        }
    }

    animate();
}

=======
>>>>>>> development:documents/old code/old_js/frontend/index.js
//Orbit camera
var controls = new OrbitControls(camera, renderer.domElement);
controls.addEventListener('change', animate);

<<<<<<< HEAD:frontend/js/index.js
function animate() {
    renderer.render(scene, camera);
    spotLight.position.set(
        camera.position.x + 10,
        camera.position.y + 10,
        camera.position.z + 10
    )

    models.forEach((item, i) => {
        console.log(item);
        var posVector = item.data.posVector;
        item.position.set(posVector.x, posVector.y, posVector.z);
        controls.target.set(posVector.x, posVector.y, posVector.z);
    })

    requestAnimationFrame(animate);
=======
/*
function run(item) {
    const group = scene.getObjectByName(item.id);
    const unit = group.children[0];
    const cone = group.children[1];
    var headVector = getBaseFromHeading(item, 100);

    if (item.target.length > 0) {
        for (var i = 0; i < item.target.length; i++) {
            try {
                const target = scene.getObjectByName(item.target[i].id);
                const sphere = target.children[2];
                var relativeVector = new THREE.Vector3().copy(target.position);
                relativeVector.sub(group.position);
                headVector.copy(target.position);

                sphere.material.opacity = 0.25;
            } catch (e) {

            }
        }
    } else {

    }

    //Makes the object face the velocity of the object
    var velVector = new THREE.Vector3(item.velVector.x, item.velVector.z, item.velVector.y);
    var posVector = new THREE.Vector3(item.posVector.x, item.posVector.z, item.posVector.y);
    // Creates the modified vector that determines where the object is going to face.
    var modVector = posVector.add(velVector);
    //Weird Quaternion stuff
    var velMatrix = new THREE.Matrix4();
    var velQuaternion = new THREE.Quaternion();
    velMatrix.lookAt(modVector, group.position, group.up);
    //Creates the amount to rotate given the matrix
    velQuaternion.setFromRotationMatrix(velMatrix);
    //Makes the unit rotate towards the given Quaternion
    unit.quaternion.rotateTowards(velQuaternion, Math.PI / 2);

    //This block of code represents what the object is locked on to.
    var rotationMatrix = new THREE.Matrix4();
    var targetQuaternion = new THREE.Quaternion();
    //console.log(headVector);
    rotationMatrix.lookAt(headVector, group.position, group.up);
    targetQuaternion.setFromRotationMatrix(rotationMatrix);
    cone.quaternion.rotateTowards(targetQuaternion, Math.PI / 2);
>>>>>>> development:documents/old code/old_js/frontend/index.js
}
*/

export { renderer, scene, camera, spotLight, getModel };