'use strict'
import * as THREE from './../build/three.module.js';

//Simplified lights to rgb for better control
function newAmbientLight(hex, intensity){
    return new THREE.AmbientLight(hex, intensity);
}

function newHemisphereLight(hex, hex2, intensity) {
    return new THREE.HemisphereLight(hex, hex2, intensity);
}

function newSpotLight(hex, intensity){
    return new THREE.SpotLight(hex, intensity);
}

function newDirectionalLight(hex, intensity){
    return new THREE.DirectionalLight(hex, intensity);
}

function newPointLight(hex, intensity){
    return new THREE.PointLight(hex, intensity);
}

export { newAmbientLight, newHemisphereLight, newSpotLight, newDirectionalLight, newPointLight };