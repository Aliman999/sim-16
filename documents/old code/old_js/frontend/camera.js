'use strict'
import * as THREE from './../build/three.module.js';

//Camera initialization function, points at 0, 0, 0
function createCamera(FOV, window, minRenderDistance, maxRenderDistance, x, y, z){
    const camera = new THREE.PerspectiveCamera(FOV, window.innerWidth / window.innerHeight, minRenderDistance, maxRenderDistance);
    camera.rotation.y = 45/180*Math.PI;
    camera.position.set(x, y, z);
    return camera;
};

export { createCamera };