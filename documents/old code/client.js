//Run this to communicate with the simulation in another console
'use strict'
const Socket = require('./js/socket.js');
require('./js/cycle.js');

var socket;

function init(){
    socket = new Socket();
    socket.startClient().then((status) => {
        status = JSON.parse(status);
        console.log("CLIENT <- SERVER: "+status.data);
        socket.ws.on('message', (msg) => {
            msg = JSON.retrocycle(JSON.parse(msg)).toString();
            console.log(msg.data);
        });
    }).catch((e) => {
        console.log("ERROR: " + e.message);
        setTimeout(() => {
            init();
        }, 1000);
    })
}
init();