'use strict'
const events = require('events');
const vector = require('./vector.js');
const CONFIG = require("./config.js");

class Poi {
	//identifier variables
	name = '';
	//locational variables
	posVector = new vector.Vector(0,0,0);
	//color
	rbgVector = {'r':0,'g':0,'b':0};

	//Radio Events
	events = new events.EventEmitter();
	radio = [];
	heading = 0;
	
	//unit array that stores who belongs to the poi
	//type arrays
	planes = [];
	vehicles = [];
	centers = [];
	radars = [];
	
	constructor(name, x_pos, y_pos, z_pos){
		this.posVector.x = x_pos;
		this.posVector.y = y_pos;
		this.posVector.z = z_pos;
		this.rbgVector.r = Math.floor(x_pos) % 256;
		this.rbgVector.g = Math.floor(y_pos) % 256;
		this.rbgVector.b = Math.floor(z_pos) % 256;
		this.name = name;
	}
	
	//positioning functions 
	calculateRotVector(heading){
		let norm = new vector.Vector(1,0,0);
		//convert degrees to rads
		let rads = heading * (Math.PI / 180);
		//we rotate our normal on our modified heading
		let shiftX = (norm.x * Math.cos(rads)) - (norm.y * Math.sin(rads));
		let shiftY = (norm.x * Math.sin(rads)) + (norm.y * Math.cos(rads));
		norm.x = shiftX;
		norm.y = shiftY;
		return norm;
	}
	
	//radio functions
	addRadio = function(freq, band){
		this.radio.push(freq+band);
	}
	removeRadio = function(freq, band){
		var index = this.radio.indexOf(freq+band);
		this.radio.splice(idex, 1);
	}
	
	//spawning functions
	/*
		unit	the unit object to be spawned
		category	the poi array that stores the specific type of unit
		unitCategory	the theater categorized array that stores the unit
		theaterUnits	the overall array that stores every unit in the theater
	*/
	spawnUnit(unit, category, unitCategory, theaterUnits) {
		new Promise(callback => {
			let position_x = this.posVector.x;
			let position_y = this.posVector.y;
			let position_z = this.posVector.z;
			unit.posVector = new vector.Vector(position_x,position_y,position_z);
			unit.addRadio(CONFIG.RADIO_FREQ.freq, CONFIG.RADIO_FREQ.band);
			unit.link = this.link;
			category.push(unit);			//pushes to poi type arrays
			unitCategory.air.push(unit);	//pushes to theater unitCategory
			theaterUnits.push(unit);		//pushes to overall units in theater
			callback;
		});
	}
}

module.exports = Poi;