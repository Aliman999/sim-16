'use strict'
const FIELD_SIZE = 750;
const MAX_CONNECTION_RANGE = 1000;

const RADIO_FREQ = { freq: 251.00, band: "mhz" };
const KILL_DISTANCE = 7;

const LOCK_LATENCY = 1;

module.exports = { FIELD_SIZE, MAX_CONNECTION_RANGE, RADIO_FREQ, KILL_DISTANCE, LOCK_LATENCY };