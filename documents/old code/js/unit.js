<<<<<<< HEAD:js/unit.js
'use strict'
const events = require('events');
const CONFIG = require("./config.js");
const vector = require('./vector.js');
class Unit {
	//identifier variables
	name = '';
	//locational variables
	posVector = new vector.Vector(0,0,0);
	//color
	rbgVector = {'r':0,'g':0,'b':0};
	//movement descriptors
	velVector = new vector.Vector(0,0,0);
	config = CONFIG;
	target = null;
	//waypoint queue
	waypoints = new Array();
	currentWaypoint = new vector.Vector(0,0,0);
	//link 16 variables LEGACY | NOW USING THEATER.BLUFOR.LINK
	link_connections = new Array();

	//Radio Events
	events = new events.EventEmitter();
	radio = [];
	
	constructor(name, x_pos, y_pos, z_pos){
		this.posVector.x = x_pos;
		this.posVector.y = y_pos;
		this.posVector.z = z_pos;
		this.rbgVector.r = Math.floor(x_pos) % 256;
		this.rbgVector.g = Math.floor(y_pos) % 256;
		this.rbgVector.b = Math.floor(z_pos) % 256;
		this.name = name;
	}
	
	//connection functions (legacy)
	addNewConnection = function (obj) {
		var connection = { object: obj, distance: this.calculateDistance(obj), heading: this.calculateHeading(obj) };
		this.link_connections.push(connection);
	}
	removeConnection(obj) {
		let index = this.link_connections.indexOf(obj);
		this.link_connections.splice(index, 1);
	}
	updateConnection = function (obj) {
		for (var x = 0; x < this.link_connections; x++) {
			if (this.link_connections[x] === obj) {
				var connection = { object: obj, distance: this.calculateDistance(obj), heading: this.calculateHeading(obj) };
				this.link_connections[x] = connection;
			}
		}
	}
	
	//targeting functions
	lock = function (id, posVector, heading, distance) {
		this.target = {
			id,		 //Obj ID
			posVector,   //X, Y, Z
			heading,     //HDG in Degrees
			distance     //Distance in KM
		};
		this.lockAttempt = 0;
	}
	unlock = function () {
		this.target = null;
	}
	
	//positioning functions 
	calculateDistance(obj) {
        return Math.sqrt(
            Math.pow((obj.posVector.x - this.posVector.x), 2) +
            Math.pow((obj.posVector.y - this.posVector.y), 2) +
            Math.pow((obj.posVector.z - this.posVector.z), 2)
        );
    }
	
	calculateHeading = function (obj) {
		Math.degrees = function (x, y) {
			var radians = Math.atan2(x, y);
			if (x > 0 && y > 0) {
				return (radians * 180 / Math.PI) % 360;
			} else if (x > 0 && y <= 0) {
				return (radians * 180 / Math.PI) % 360;
			} else if (x <= 0 && y < 0) {
				return ((radians * 180 / Math.PI) + 360) % 360;
			} else {
				return ((radians * 180 / Math.PI) + 360) % 360;
			}
		};
		return Math.degrees((obj.posVector.y - this.posVector.y), (obj.posVector.x - this.posVector.x));
	}

	calculateRotVector(heading){
		let norm = new vector.Vector(1,0,0);
		//convert degrees to rads
		let rads = heading * (Math.PI / 180);
		//we rotate our normal on our modified heading
		let shiftX = (norm.x * Math.cos(rads)) - (norm.y * Math.sin(rads));
		let shiftY = (norm.x * Math.sin(rads)) + (norm.y * Math.cos(rads));
		norm.x = shiftX;
		norm.y = shiftY;
		return norm;
	}
	
	//radar functions
	getBaseFromHeading(heading, length){
		/* 
			the goal is to calculate a horizontal base to the unit such that it points in the direction of the heading
			we can therefore ignore the z axis, setting it to 0 for calculation purposes
		*/
		//we start with a generic vector on the unit circle pointing to 1 in the x direction
		//this is what we will rotate in order to find the direction of the heading
		let norm = new vector.Vector(1,0,0);
		//convert degrees to rads
		let rads = heading * (Math.PI / 180);
		//we rotate our normal on our modified heading
		let shiftX = (norm.x * Math.cos(rads)) - (norm.y * Math.sin(rads));
		let shiftY = (norm.x * Math.sin(rads)) + (norm.y * Math.cos(rads));
		norm.x = shiftX;
		norm.y = shiftY;
		//we now extend out our base using the length of our radar scan
		norm = vector.multiplyVector(norm,length);
		//finally we add this new base to our current position to get it relative to the unit
		let baseVector = vector.addVector(this.posVector, norm);
		return baseVector;
	}
	targetInCone(startingPoint, angle, target, currentHeading, beamLength) {	
		/* startingPoint is our units position 
		   angle is the max angle our beam fires at in both directions
		   target is our current target to query 
		   currentHeading is the units current heading, which is what we will use as a base for measuring
		   beamLength is the distance our beam can travel
		   
		   essentially we first calculate the bases of our heading, and the +/- heading scan
		   we then see if our targets cords are somewhere between these amounts
		   if it is we check that its angle is within our target
		   
		*/
		//step 1, calculating the bases of our headings 
		let minusBase = this.getBaseFromHeading(currentHeading - angle, beamLength);
		let plusBase = this.getBaseFromHeading(currentHeading + angle, beamLength);
		let mainBase = this.getBaseFromHeading(currentHeading, beamLength);
		//we can now check if our target is within these parameters
		if(Math.min(mainBase.x, startingPoint.x) <= target.posVector.x && target.posVector.x <= Math.max(startingPoint.x, mainBase.x)){
			if(Math.min(minusBase.y, plusBase.y) <= target.posVector.y && target.posVector.y <= Math.max(plusBase.y, minusBase.y)){
				//we can now check the angle since we know it is within our bounds
				//we first need to convert any of our bases, and the targets pos to a unit vector
				let plusUnit = vector.unitVector(plusBase);
				let targetUnit = vector.unitVector(target.posVector);
				//now we can use the dot product to get a cos
				let cosDot = vector.dotProduct(targetUnit, plusUnit);
				//now we get the arc cos, which is how we test the angle in comparison to our angle
				let arcCosDot = Math.acos(cosDot);
				//since we work in degrees we convert this radians result
				let dotDegrees = arcCosDot * (180 / Math.PI);
				//finally our comparison to see if our plane is in fact, within our sights
				if(dotDegrees < angle){
					return true;
				}
				//if the above fails, our target is too high up
				return false;
			}
			//if target is out of Y position, we return false
			return false;
		}
		//target falls out of X position, return false
		return false;
	}
	
	//causes velocity to change biased towards a target position
	goToTarget(targetPosition,maxSpeed,moveMod,bounds,fieldSize){
		let cohVector = vector.subVector(targetPosition,this.posVector);
		let modVelVector = this.velVector;
		//adds to velocity vector
		modVelVector = vector.addVector(modVelVector, cohVector);
		//dampen movement as we approach the target
		modVelVector.x *= Math.abs((targetPosition.x - this.posVector.x) / 1);
		modVelVector.y *= Math.abs((targetPosition.y - this.posVector.y) / 1);
		modVelVector.z *= Math.abs((targetPosition.z - this.posVector.z) / 1);
		//binds the speed to a specific limit
		modVelVector = vector.boundSpeed(modVelVector, maxSpeed);
		//checks if plane going out of bounds
		modVelVector = vector.addVector(modVelVector, vector.boundPos(this.posVector, moveMod, bounds, fieldSize));
		return modVelVector;
	}
	//causes velocity to change biased away from a target position
	avoidPosition(targetPosition,maxSpeed,moveMod,bounds,fieldSize){
		let cohVector = vector.subVector(targetPosition,this.posVector);
		let modVelVector = this.velVector;
		cohVector.x *= -1;
		cohVector.y *= -1;
		cohVector.z *= -1;
		//adds to velocity vector
		modVelVector = vector.addVector(modVelVector, cohVector);
		//checks if plane going out of bounds
		modVelVector = vector.addVector(modVelVector, boundPos(this.posVector, moveMod, bounds, fieldSize));
		//binds the speed to a specific limit
		modVelVector = boundSpeed(modVelVector, maxSpeed);
		return modVelVector;
	}
	//causes velocity vector to shift a specified amount
	injectRandomMovement(scale){
		let randomVector = new vector.Vector(0,0,0);
		let modifiedVel = new vector.Vector(0,0,0);
		randomVector.x = (Math.random() * (scale * 2)) - scale;
		randomVector.y = (Math.random() * (scale * 2)) - scale;
		randomVector.z = (Math.random() * (scale * 2)) - scale;
		modifiedVel = vector.addVector(this.velVector, randomVector);
		return modifiedVel;
	}
	//call to add current velocity to current position
	performMovement(){
		this.posVector = vector.addVector(this.posVector, this.velVector);
	}
	
	//radio functions
	addRadio = function(freq, band){
		this.radio.push(freq+band);
	}
	removeRadio = function(freq, band){
		var index = this.radio.indexOf(freq+band);
		this.radio.splice(idex, 1);
	}
	
	//waypoint functions
	addWaypoint(newPosition){
		this.waypoints.push(newPosition);
	}
	getNextWaypoint(){
		return this.waypoints.shift();
	}
}

=======
'use strict'
const events = require('events');
const CONFIG = require("./config.js");
const vector = require('./vector.js');
class Unit {
	//identifier variables
	name = '';
	//model name for client render
	model = '';
	//ID random ID to reference specific object
	id = makeid(24);
	//locational variables
	posVector = new vector.Vector(0,0,0);
	//color
	rbgVector = {'r':0,'g':0,'b':0};
	//movement descriptors
	velVector = new vector.Vector(0,0,0);
	config = CONFIG;
	target = null;
	//waypoint queue
	waypoints = new Array();
	currentWaypoint = new vector.Vector(0,0,0);
	//link 16 variables LEGACY | NOW USING THEATER.BLUFOR.LINK
	link_connections = new Array();

	//Radio Events
	events = new events.EventEmitter();
	radio = [];
	
	constructor(name, x_pos, y_pos, z_pos){
		this.posVector.x = x_pos;
		this.posVector.y = y_pos;
		this.posVector.z = z_pos;
		this.rbgVector.r = Math.floor(x_pos) % 256;
		this.rbgVector.g = Math.floor(y_pos) % 256;
		this.rbgVector.b = Math.floor(z_pos) % 256;
		this.name = name;
	}
	
	//connection functions (legacy)
	addNewConnection = function (obj) {
		var connection = { object: obj, distance: this.calculateDistance(obj), heading: this.calculateHeading(obj) };
		this.link_connections.push(connection);
	}
	removeConnection(obj) {
		let index = this.link_connections.indexOf(obj);
		this.link_connections.splice(index, 1);
	}
	updateConnection = function (obj) {
		for (var x = 0; x < this.link_connections; x++) {
			if (this.link_connections[x] === obj) {
				var connection = { object: obj, distance: this.calculateDistance(obj), heading: this.calculateHeading(obj) };
				this.link_connections[x] = connection;
			}
		}
	}
	
	//targeting functions
	lock = function (id, posVector, heading, distance) {
		this.target = {
			id,		 //Obj ID
			posVector,   //X, Y, Z
			heading,     //HDG in Degrees
			distance     //Distance in KM
		};
		this.lockAttempt = 0;
	}
	unlock = function () {
		this.target = null;
	}
	
	//positioning functions 
	calculateDistance(obj) {
        return Math.sqrt(
            Math.pow((obj.posVector.x - this.posVector.x), 2) +
            Math.pow((obj.posVector.y - this.posVector.y), 2) +
            Math.pow((obj.posVector.z - this.posVector.z), 2)
        );
    }
	
	calculateHeading = function (obj) {
		Math.degrees = function (x, y) {
			var radians = Math.atan2(x, y);
			if (x > 0 && y > 0) {
				return (radians * 180 / Math.PI) % 360;
			} else if (x > 0 && y <= 0) {
				return (radians * 180 / Math.PI) % 360;
			} else if (x <= 0 && y < 0) {
				return ((radians * 180 / Math.PI) + 360) % 360;
			} else {
				return ((radians * 180 / Math.PI) + 360) % 360;
			}
		};
		return Math.degrees((obj.posVector.y - this.posVector.y), (obj.posVector.x - this.posVector.x));
	}

	calculateRotVector(heading){
		let norm = new vector.Vector(1,0,0);
		//convert degrees to rads
		let rads = heading * (Math.PI / 180);
		//we rotate our normal on our modified heading
		let shiftX = (norm.x * Math.cos(rads)) - (norm.y * Math.sin(rads));
		let shiftY = (norm.x * Math.sin(rads)) + (norm.y * Math.cos(rads));
		norm.x = shiftX;
		norm.y = shiftY;
		return norm;
	}
	
	//radar functions
	getBaseFromHeading(heading, length, altitude){
		/* 
			the goal is to calculate a horizontal base to the unit such that it points in the direction of the heading
			we can therefore ignore the z axis, setting it to 0 for calculation purposes
		*/
		//we start with a generic vector on the unit circle pointing to 1 in the x direction
		//this is what we will rotate in order to find the direction of the heading
		let norm = new vector.Vector(1,0,0);
		//convert degrees to rads
		let rads = heading * (Math.PI / 180);
		//we rotate our normal on our modified heading
		let shiftX = (norm.x * Math.cos(rads)) - (norm.y * Math.sin(rads));
		let shiftY = (norm.x * Math.sin(rads)) + (norm.y * Math.cos(rads));
		norm.x = shiftX;
		norm.y = shiftY;
		//we now extend out our base using the length of our radar scan
		norm = vector.multiplyVector(norm, length);
		//finally we add this new base to our current position to get it relative to the unit
		let baseVector = vector.addVector(this.posVector, norm);
		baseVector.z = altitude;
		return baseVector;
	}
	targetInCone(startingPoint, angle, target, currentHeading, beamLength, altitude) 
	{
		angle = angle / 2;
		//step 1, calculating the bases of our headings 
		let mainBase = this.getBaseFromHeading(currentHeading, beamLength, altitude);
		//step 2 is to simply see if the plane is out of range of our base
		if(vector.vectorLength(mainBase,startingPoint) < vector.vectorLength(target.posVector,startingPoint)){
			return false;
		}
		//we can now check the angle since we know it is within our bounds
		//we first need to convert any of our bases, and the targets pos to a unit vector
		let plusUnit = vector.unitVector(mainBase, startingPoint);
		let targetUnit = vector.unitVector(target.posVector, startingPoint);
		//now we can use the dot product to get a cos
		let cosDot = vector.dotProduct(targetUnit, plusUnit);
		//now we get the arc cos, which is how we test the angle in comparison to our angle
		let arcCosDot = Math.acos(cosDot);
		//since we work in degrees we convert this radians result
		let dotDegrees = arcCosDot * (180 / Math.PI);
		//finally our comparison to see if our plane is in fact, within our sights
		if(dotDegrees < angle){
			return true;
		}
		//if the above fails, our target is out of angular range
		return false;
	}
	
	//causes velocity to change biased towards a target position
	goToTarget(targetPosition,maxSpeed,moveMod,bounds,fieldSize){
		let cohVector = vector.subVector(targetPosition,this.posVector);
		let modVelVector = this.velVector;
		//adds to velocity vector
		modVelVector = vector.addVector(modVelVector, cohVector);
		//dampen movement as we approach the target
		modVelVector.x *= Math.abs((targetPosition.x - this.posVector.x) / 1);
		modVelVector.y *= Math.abs((targetPosition.y - this.posVector.y) / 1);
		modVelVector.z *= Math.abs((targetPosition.z - this.posVector.z) / 1);
		//binds the speed to a specific limit
		modVelVector = vector.boundSpeed(modVelVector, maxSpeed);
		//checks if plane going out of bounds
		modVelVector = vector.addVector(modVelVector, vector.boundPos(this.posVector, moveMod, bounds, fieldSize));
		return modVelVector;
	}
	//causes velocity to change biased away from a target position
	avoidPosition(targetPosition,maxSpeed,moveMod,bounds,fieldSize){
		let cohVector = vector.subVector(targetPosition,this.posVector);
		let modVelVector = this.velVector;
		cohVector.x *= -1;
		cohVector.y *= -1;
		cohVector.z *= -1;
		//adds to velocity vector
		modVelVector = vector.addVector(modVelVector, cohVector);
		//checks if plane going out of bounds
		modVelVector = vector.addVector(modVelVector, boundPos(this.posVector, moveMod, bounds, fieldSize));
		//binds the speed to a specific limit
		modVelVector = boundSpeed(modVelVector, maxSpeed);
		return modVelVector;
	}
	//causes velocity vector to shift a specified amount
	injectRandomMovement(scale){
		let randomVector = new vector.Vector(0,0,0);
		let modifiedVel = new vector.Vector(0,0,0);
		randomVector.x = (Math.random() * (scale * 2)) - scale;
		randomVector.y = (Math.random() * (scale * 2)) - scale;
		randomVector.z = (Math.random() * (scale * 2)) - scale;
		modifiedVel = vector.addVector(this.velVector, randomVector);
		return modifiedVel;
	}
	//call to add current velocity to current position
	performMovement(){
		this.posVector = vector.addVector(this.posVector, this.velVector);
	}
	
	//radio functions
	addRadio = function(freq, band){
		this.radio.push(freq+band);
	}
	removeRadio = function(freq, band){
		var index = this.radio.indexOf(freq+band);
		this.radio.splice(idex, 1);
	}
	
	//waypoint functions
	addWaypoint(newPosition){
		this.waypoints.push(newPosition);
	}
	getNextWaypoint(){
		return this.waypoints.shift();
	}
}
//Creates unit ID
function makeid(length) {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() *
			charactersLength));
	}
	return result;
}

>>>>>>> development:documents/old code/js/unit.js
module.exports = Unit;