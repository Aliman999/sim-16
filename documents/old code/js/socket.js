'use strict'
const { WebSocketServer, WebSocket } = require('ws');

class Socket {
    constructor(port = 8080) {
        this.port = port;
    }

    // If your running this object as a server this will transform this object into a server only enviornment.
    startServer = async ()=>{
        delete this.client;
        delete this.startServer;
        delete this.startClient;
        return await this.server.start();
    }
    // If your running this object as a client this will transform this object into a client only enviornment.
    startClient = async () => {
        delete this.server;
        delete this.startServer;
        delete this.startClient;
        return await this.client.start();
    }

    server = {
        start: () => {
            return new Promise((callback, reject) => {
                try{
                    this.wss = new WebSocketServer({ port: this.port, clientTracking: true });

                    //This will turn all messages (objects) into an event so the websocket listens for a particular event type instead of parsing the message into arguments each time.
                    function toEvent(message) {
                        var event = JSON.parse(message);
                        event = Object.values(event);
                        this.emit(...event);
                    }

                    var heartBeat = () => {
                        this.isAlive = true;
                    }

                    const interval = setInterval(function ping() {
                        //inactive
                    }, 30000);

                    /*
                    When the client connects the server will send it a pong and the client by web socket standard will respond with a ping to verify life.
                    The query event that the server is listening for is for multiplayer sessions,
                    In the future this will expand capabilities to other aspects of the game.
                    */
                    this.wss.on('connection', function connection(ws) {
                        ws.on('pong', heartBeat)
                        ws.on('message', toEvent)
                            .on('query', (msg) => {
                                console.log("CLIENT -> SERVER: " + msg);
                            })
                        ws.send(JSON.stringify({ data: "Connected", status: 200, message: "success" }));
                    });
                    callback(true);
                }catch(e){
                    reject(e);
                }
            })
        },
        //This function allows a standard object to be passed and a optional client object (ws) to send data to a particular client.
        send: (msg, client = null) => {
            return new Promise((callback, reject) => {
                try {
                    if (client === null) {
                        this.wss.clients.forEach(wsClient => {
                            wsClient.send(JSON.stringify({ data: msg, status: 200, message: "success" }));
                        })
                    } else {
                        this.wss.clients.forEach(wsClient => {
                            if (client === wsClient) {
                                wsClient.send(JSON.stringify({ data: msg, status: 200, message: "success" }));
                                return true;
                            }
                        })
                    }
                    callback(true);
                }catch(e){
                    reject(e);
                }
            })
        },
        //This function will clean dead connections and renew live connections
        clean: setInterval(() => {
            function noop() { };
            this.wss.clients.forEach(function each(ws) {
                if (ws.isAlive === false) return ws.terminate();

                ws.isAlive = false;
                ws.ping(noop);
            });
        }, 30000)
    }
    client = {
        // Starts the client listening to the server location and listens for message updates.
        start: () => {
            return new Promise((callback, reject) => {
                this.ws = new WebSocket('ws://localhost:' + this.port);

                this.ws.on('error', (error)=>{
                    reject(error);
                })

                var heartBeat = () => {
                    clearTimeout(this.pingTimeout);
                    this.pingTimeout = setTimeout(() => {
                        this.terminate();
                    }, 30000 + 1000);
                }

                this.ws.on('ping', heartBeat);

                this.ws.on('open', () => {
                    console.log("CLIENT -> SERVER: CONNECT");
                });

                this.ws.on('message', (msg) => {
                    msg = msg.toString();
                    callback(msg);
                });
            })
        },
        // Sends data back to the server this will be for multiplayer services later down the line.
        send: (msg) =>{
            return new Promise((callback, reject) => {
                try{
                    this.ws.send(JSON.stringify({ event: 'query', data: msg }))
                    callback(true);
                }catch(e){
                    reject(e);
                }
            })
        },
    }
}

module.exports = Socket;