'use strict'
const Unit = require('./unit.js');
const Missile = require('./missile.js');
const random = require("./random.js");
class Vehicle extends Unit {
    model = "";
    range = 0; // Range is determined by the weapons on the vehicle.
    weapons = [];
    feeding = [];
    loaded = null;

    delay = 60; //Frame Delay between shots on target
    interval = 60; // Set to 60 because when the vehicle gets a target it will fire immediately.

    target = null;
    lockAttempt = 0; // Used to clean up target locks. While running, if the vehicle is not fed a target in x frames it will drop lock.

    heading = random(360);

    mode = "standby";
    power = true;

    //In the future the model will decide what types of missiles its using and the missiles will determine the vehicles range etc.
    constructor(name, x, y, model) {
        super(name, x, y, 0);
        this.model = model;
        this.scanAngle = 20;
    }

    //Reloads vehicle with the next weapon in the array.
    reload = function(){
        if(this.weapons.length > 0){
            var weapon = this.weapons[0];
            this.range = weapon.range;
            this.loaded = weapon;

            let index = this.weapons.indexOf(this.loaded);
            this.weapons.splice(index, 1);
        }
    }

    // Passes target information sourced from a radar or center onto the missile recently fired. This will stop feeding the missile information if the missile has a radar seeker of its own.
    feed = function(){
        for(var i = 0; i < this.feeding.length; i++){
            var obj = this.feeding[i];
            // The missile will change modes when fired to active meaning its flying and burning fuel guided by the vehicle.
            if (obj.mode == "active") {
                var heading = obj.calculateHeading(this.target);
                var distance = obj.calculateDistance(this.target);
                obj.lock(this.target.id, this.target.posVector, heading, distance);
            } else {
                //If the missile dies or goes 'pitbull' aka seeking out the target with its own radar then the vehicle will reload.
                this.reload();
                let index = this.feeding.indexOf(obj);
                this.feeding.splice(index, 1);
            }
        }
    }

    /* 
    This loads the missile with the fire event listener and gives the missile a lock.
    It will then tell the missile to fire and this vehicle will tell the theater that it fired and pushes the fired weapon to the theater to theater.units to render.
    The event listner in theater will pass the object this.events.emit("fire", ->this.loaded<-); to the theater.
    */
    fire = function () {
        if (this.loaded) {
            this.loaded.events.once("fire", () => {
                this.events.emit("fire", this.loaded);
            });

            this.loaded.lock(this.target.id, this.target.posVector, this.target.heading, this.target.distance);
            this.loaded.fire();
            this.loaded.events.off("fire", () => {});


            this.feeding.push(this.loaded);
            this.loaded = null;

            return true;
            
        } else {
            this.mode = "reloading";
            return false;
        }
    }

    /*
    Vehicles runtime

    The first frame of the vehicle sets up weapons and loades a weapon. I'm not exactly sure why i chose this approach. I feel as though the if statement this.setup === true is a waste of time.
    */
    run = function () {
        if (this.setup === true) {
            this.interval++;
            if (this.target) {
                /*
                Every frame the vehicle checks if it was fed target information. If the target information was given then it face the missiles towards the target.
                This was in an attempt to get smooth vector lines to the target but it doesnt work but we're keeping it for when the face of the missile matters.
                */
                this.mode = "active";
                if(this.loaded){
					this.loaded.heading = this.target.heading;
                }
				this.heading = this.target.heading;
                for(var i = 0; i < this.weapons.length; i++){
                    this.weapons[i].heading = this.target.heading;
                }

                //This is a redundant safe feeding function to ensure that missiles fired by this vehicle are constantly fed updated target information.
                this.feed();

                // Vectors for canvas drawing to show target marking
				/*
                this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * this.target.distance;
                this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * this.target.distance;
				*/
                /*
                Every time run is called this.interval++ is the delay for the next missile to be loaded and fired.
                This vehicle will only reload when it's active missile is either dead or 'pitbull' aka seeking its own target with its independent radar.
                */
                if (this.loaded) {
                    this.loaded.heading = this.target.heading;
                    if (this.target.distance < this.loaded.range && this.interval >= this.delay) {
                        this.interval = 0;
                        this.fire();
                    }
                }
                
                //Cleans up lock incase it is not fed new target information.
                if (this.lockAttempt > this.config.LOCK_LATENCY) {
                    this.unlock();
                    this.lockAttempt = 0;
                } else {
                    this.lockAttempt++;
                }
            }else{
					 /*
                this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * 0;
                this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * 0;
					 */
                this.mode = "standby";
            }
        } else {
            //Setup
            for (var x = 0; x < 6; x++) {
                var obj = new Missile("NASAMS - "+x, this.posVector.x, this.posVector.y, this.posVector.z, "NASAMS");
                this.range = obj.range;
                this.weapons.push(obj);
            }
            this.loaded = this.weapons[0];
				/*
            this.velVector.x = Math.cos(this.heading * (Math.PI / 180)) * 0;
            this.velVector.y = Math.sin(this.heading * (Math.PI / 180)) * 0;
				*/
            this.setup = true;
        }
    }
}

module.exports = Vehicle;