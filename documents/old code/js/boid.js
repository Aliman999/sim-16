const vector = require('./vector.js');
function moveAllBoids(planeArray, fieldSize, maxSpeed){
	let v1 = {};
	let v2 = {};
	let v3 = {};
	let totalVector = {};
	for(let i=0;i<planeArray.length;i++){
		v1 = cohesion(planeArray[i], planeArray);
		v2 = separation(planeArray[i], planeArray);
		v3 = alignment(planeArray[i], planeArray);
		totalVector = vector.addVector(v1, v2);
		totalVector = vector.addVector(totalVector, v3);
		planeArray[i].velVector = vector.addVector(planeArray[i].velVector, totalVector);
		
		limitSpeed(planeArray[i], maxSpeed);
		
		planeArray[i].posVector = vector.addVector(planeArray[i].posVector, planeArray[i].velVector);
		
		boundPosition(planeArray[i], fieldSize);
	}
}
//rule 1
function cohesion(currentPlane, planeArray){
	let cohVector = {'x':0,'y':0,'z':0};
	for(let i=0;i<planeArray.length;i++){
		if(planeArray[i] != currentPlane && currentPlane.calculateDistance(planeArray[i]) < 300){
			cohVector = vector.addVector(cohVector, planeArray[i].posVector);
		}
	}
	cohVector = vector.divideVector(cohVector, (planeArray.length - 1));
	let result = vector.subVector(cohVector, currentPlane.posVector);
	result = vector.divideVector(result, 100);
	return result;
}
//rule 2
function separation(currentPlane, planeArray){
	let sepVector = {'x':0,'y':0,'z':0};
	for(let i=0;i<planeArray.length;i++){
		if(planeArray[i] != currentPlane && currentPlane.calculateDistance(planeArray[i]) < 300){
			if(currentPlane.calculateDistance(planeArray[i]) < 200){
				let firstSub = vector.subVector(currentPlane.posVector, planeArray[i].posVector);
				sepVector = vector.subVector(sepVector, firstSub);
			}
		}
	}
	return sepVector;
}
//rule 3
function alignment(currentPlane, planeArray){
	let aliVel = {'x':0,'y':0,'z':0};
	for(let i=0;i<planeArray.length;i++){
		if(planeArray[i] != currentPlane && currentPlane.calculateDistance(planeArray[i]) < 300){
			aliVel = vector.addVector(aliVel, planeArray[i].velVector);
		}
	}
	aliVel = vector.divideVector(aliVel,(planeArray.length - 1));
	let subStep = vector.subVector(aliVel, currentPlane.velVector);
	let result = vector.divideVector(subStep, 8);
	return result;
}
/*
	http://www.kfish.org/boids/pseudocode.html
	https://en.wikipedia.org/wiki/Boids
	https://processing.org/examples/flocking.html
*/
//helper functions
function limitSpeed(plane, maxSpeed){
	if(Math.abs(plane.velVector.x) > maxSpeed){
		plane.velVector.x = (plane.velVector.x / Math.abs(plane.velVector.x)) * maxSpeed;
	}
	if(Math.abs(plane.velVector.y) > maxSpeed){
		plane.velVector.y = (plane.velVector.y / Math.abs(plane.velVector.y)) * maxSpeed;
	}
	if(Math.abs(plane.velVector.z) > maxSpeed){
		plane.velVector.z = (plane.velVector.z / Math.abs(plane.velVector.z)) * maxSpeed;
	}
}
function boundPosition(plane, fieldSize){
	if(plane.posVector.x < Math.floor(Math.random() * 25) + 10){
		plane.posVector.x = 35;
		plane.velVector.x *= -1;
	}
	else if(plane.posVector.x > fieldSize - Math.floor(Math.random() * 25)){
		plane.posVector.x = fieldSize - 35;
		plane.velVector.x *= -1;
	}
	if(plane.posVector.y < Math.floor(Math.random() * 25) + 10){
		plane.posVector.y = 35;
		plane.velVector.y *= -1;
	}
	else if(plane.posVector.y > fieldSize - Math.floor(Math.random() * 25)){
		plane.posVector.y = fieldSize - 35;
		plane.velVector.y *= -1;
	}
	if(plane.posVector.z < Math.floor(Math.random() * 25) + 10){
		plane.posVector.z = 35;
		plane.velVector.z *= -1;
	}
	else if(plane.posVector.z > fieldSize - Math.floor(Math.random() * 25)){
		plane.posVector.z = fieldSize - 35;
		plane.velVector.z *= -1;
	}
}
module.exports = {
	moveAllBoids
}