'use strict'
const MAX_MOVE_DISTANCE = 5;
const MAX_VEL_CHANGE = 5;
const Unit = require('./unit.js');
const vector = require('./vector.js');
class Plane extends Unit{
	range = 50;
	scanAngle = 10;
	heading = 0;
	scanHeading = 0;
	//constructor for a plane
	constructor(name, x_pos, y_pos, z_pos, model){
		super(name, x_pos, y_pos, z_pos);
		//Temporary Model Name - Spawns model on client side
		this.model = "f_16";
	}
	
	//methods
	moveRandomPos(maxSize){
		let x_move = (Math.random() * (MAX_MOVE_DISTANCE * 2)) - MAX_MOVE_DISTANCE;
		let y_move = (Math.random() * (MAX_MOVE_DISTANCE * 2)) - MAX_MOVE_DISTANCE;
		let z_move = (Math.random() * (MAX_MOVE_DISTANCE * 2)) - MAX_MOVE_DISTANCE;
		
		if(x_move + this.posVector.x >= maxSize){
			this.posVector.x -= x_move;
		}
		else if(this.posVector.x + x_move < 0){
			this.posVector.x -= x_move;
		}
		else{
			this.posVector.x += x_move;
		}
		
		if(y_move + this.posVector.y >= maxSize){
			this.posVector.y -= y_move;
		}
		else if(this.posVector.y + y_move < 0){
			this.posVector.y -= y_move;
		}
		else{
			this.posVector.y += y_move;
		}
		
		if(z_move + this.posVector.z >= maxSize){
			this.posVector.z -= z_move;
		}
		else if(this.posVector.z + z_move < 0){
			this.posVector.z -= z_move;
		}
		else{
			this.posVector.z += z_move;
		}
	}
	
	moveWithVelocity(maxSize){
		let x_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		let y_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		let z_vel = (Math.random() * (MAX_VEL_CHANGE * 2)) - MAX_VEL_CHANGE;
		
		this.velVector.x += x_vel;
		this.velVector.y += y_vel;
		this.velVector.z += z_vel;
		
		if(Math.abs(this.velVector.x) > MAX_MOVE_DISTANCE){
			this.velVector.x = (this.velVector.x / Math.abs(this.velVector.x)) * MAX_MOVE_DISTANCE;
		}
		if(Math.abs(this.velVector.y) > MAX_MOVE_DISTANCE){
			this.velVector.y = (this.velVector.y / Math.abs(this.velVector.y)) * MAX_MOVE_DISTANCE;
		}
		if(Math.abs(this.velVector.z) > MAX_MOVE_DISTANCE){
			this.velVector.z = (this.velVector.z / Math.abs(this.velVector.z)) * MAX_MOVE_DISTANCE;
		}
		
		this.posVector = vector.addVector(this.posVector, this.velVector);
		
		if(this.posVector.x >= maxSize){
			this.posVector.x = maxSize - 25;
			this.velVector.x *= -1;
		}
		else if(this.posVector.x < 0){
			this.posVector.x = 25;
			this.velVector.x *= -1;
		}

		
		if(this.posVector.y >= maxSize){
			this.posVector.y = maxSize - 25;
			this.velVector.y *= -1;
		}
		else if(this.posVector.y < 0){
			this.posVector.y = 25;
			this.velVector.y *= -1;
		}

		
		if(this.posVector.z >= maxSize){
			this.posVector.z = maxSize - 25;
			this.velVector.z *= -1;
		}
		else if(this.posVector.z < 0){
			this.posVector.z = 25;
			this.velVector.z *= -1;
		}

	}
	
	updateDistance(){
		for(let i=0;i<this.link_connections.length;i++){
			this.link_connections[i].distance = this.calculateDistance(this.link_connections[i].object);
		}
	}
	
	printPrettyPlaneInfo(){
		let output = `INFO:\nplane ${this.name}: xpos:${this.posVector.x}, ypos:${this.posVector.y}, zpos:${this.posVector.z}\nxvel: ${this.velVector.x}, yvel: ${this.velVector.y}, zvel: ${this.velVector.z}\nDISTANCES:\n`;
		for(let d=0;d<this.link_connections.length;d++){
			output += `${this.link_connections[d].object.name} : xpos:${this.link_connections[d].object.posVector.x}, ypos:${this.link_connections[d].object.posVector.y}, zpos:${this.link_connections[d].object.posVector.z}, xvel:${this.link_connections[d].object.velVector.x}, yvel:${this.link_connections[d].object.velVector.y}, zvel:${this.link_connections[d].object.velVector.z} dis:${this.link_connections[d].distance}\n`;
		}
		return output;
	}

	run(){
		if(this.target){
			this.heading = this.target.heading;
			this.velVector = this.goToTarget(this.target.posVector, 1.8, 30, 10, this.config.FIELD_SIZE);
			this.performMovement();
		}else{
			this.moveWithVelocity();
		}
	}
}

module.exports = Plane;