# Joint Tactical Information Distribution System
### https://en.wikipedia.org/wiki/Joint_Tactical_Information_Distribution_System
---

The Joint Tactical Information Distribution System (JTIDS) is an L band Distributed Time Division Multiple Access (DTDMA) network radio system used by the United States armed forces and their allies to support data communications needs, principally in the air and missile defense community.

It produces a spread spectrum signal using Frequency-shift keying (FSK) and Phase-shift keying (PSK) to spread the radiated power over a wider spectrum (range of frequencies) than normal radio transmissions. This reduces susceptibility to noise, jamming, and interception. 

In JTIDS Time Division Multiple Access (TDMA) (similar to cell phone technology), each time interval (e.g., 1 second) is divided into time slots (e.g. 128 per second). 

Together, all 1536 time slots in a 12-second interval are called a "frame". 

Each time slot is "bursted" (transmitted) at several different carrier frequencies sequentially. 

Within each slot, the phase angle of the transmission burst is varied to provide PSK. 

Each type of data to be transmitted is assigned a slot or block of slots (channel) to manage information exchanges among user participation groups. 

In traditional TDMA, the slot frequencies remain fixed from second to second (frame to frame). 

In JTIDS TDMA, the slot frequencies and/or slot assignments for each channel do not remain fixed from frame to frame but are varied in a pseudo-random manner. 

The slot assignments, frequencies, and information are all encrypted to provide computer-to-computer connectivity in support of every type of military platform to include Air Force fighters and Navy submarines.

---

 - Simulate Spread Spectrum Signaling (FSK PSK)
 - Research TDMA